from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from unittest import main, TestCase
import time


class GUITests(TestCase):

    @classmethod
    def setUpClass(self):
        self.browser = webdriver.Chrome("/Users/smrutishah/Downloads/chromedriver")
        # time.sleep(3)

    @classmethod
    def tearDownClass(self):
        # time.sleep(3)
        self.browser.close()

    def setUp(self):
        self.browser.get("https://www.landitinspace.com/")
        # self.browser.get("http://localhost:3000/")
        # time.sleep(3)

    def test_astronauts_model(self):
        # print("astro")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        # rockets_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        astronauts_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[2]')[0]
        astronauts_page.click()
        time.sleep(3)

        title = self.browser.find_elements_by_tag_name('img')
        # print(title[1].get_attribute('src'))
        self.assertTrue("/static/media/astronauts_white_nooutline.2aa2ac86.png" in title[1].get_attribute('src'))

    def test_astronauts_model2(self):
        # print("astro")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        # rockets_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        astronauts_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[2]')[0]
        astronauts_page.click()
        time.sleep(8)

        text = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[2]/span/a/a')
        self.assertTrue("Abdul" in text[0].get_attribute('href'))

    def test_satellites_model(self):
        # print("satel")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        # rockets_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        satellites_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[4]')[0]
        satellites_page.click()
        time.sleep(3)

        title = self.browser.find_elements_by_tag_name('img')
        # print(title[1].get_attribute('src'))
        self.assertTrue("/static/media/satellites_white_nooutline.4b5e0ec0.png" in title[1].get_attribute('src'))

        expected_text = "Date of Launch"
        self.assertTrue(expected_text in self.browser.page_source)

    def test_countries_model(self):
        # print("count")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        # rockets_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        countries_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[3]')[0]
        countries_page.click()
        time.sleep(1)

        search_bar = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div[1]/input')[0]
        # print(title[1].get_attribute('src'))
        self.assertEqual(search_bar.get_attribute('class'), "SearchBar")

        expected_text = "No. of Satellites"
        self.assertTrue(expected_text in self.browser.page_source)

    def test_home(self):
        # print("home")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        home_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[1]')[0]
        home_page.click()
        time.sleep(1)

        images = self.browser.find_elements_by_tag_name('img')
        # print(images[0].get_attribute('src'))
        self.assertTrue("/static/media/title.227c47f4.png" in images[0].get_attribute('src'))

    def test_astronauts_instance(self):
        # print("astro_ins")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        # rockets_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        astronauts_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[2]')[0]
        astronauts_page.click()
        time.sleep(8)

        astronauts_instance = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[4]/span/a/a/p')[0]
        astronauts_instance.click()
        time.sleep(5)

        expected_text = "Gender:"
        self.assertTrue(expected_text in self.browser.page_source)

    def test_countries_instance(self):
        # print("count_ins")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(3)

        # rockets_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        countries_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[3]')[0]
        countries_page.click()
        time.sleep(8)

        countries_instance = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[2]/td[1]/span/a/a/p/img')[0]
        countries_instance.click()
        time.sleep(5)

        #should return false if this is executed
        #expected_text = "Country:"
        expected_text = "No. of Astronauts:"
        self.assertTrue(expected_text in self.browser.page_source)

    def test_satellites_instance(self):
        # print("satel_ins")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        # rockets_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        satellites_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[4]')[0]
        satellites_page.click()
        time.sleep(8)

        satellites_instance = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[2]/span/a')[0]
        satellites_instance.click()
        time.sleep(5)

        expected_text = "Class of Orbit:"
        self.assertTrue(expected_text in self.browser.page_source)

    def test_about_links(self):
        # print("about")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        # about_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[6]')[0]
        about_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        about_page.click()
        time.sleep(1)

        expected_text = "Commits"
        self.assertTrue(expected_text in self.browser.page_source)

        gitlab_link = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div[5]/div/div[1]/a')[0].get_attribute("href")
        self.assertEqual(gitlab_link, "https://gitlab.com/jjm988/swe-group-project")

    def test_back_and_forth_pages(self):
        # print("back and forth")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        # about_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[6]')[0]
        about_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        about_page.click()
        time.sleep(1)

        team_space = self.browser.find_elements_by_tag_name('img')
        # print(team_space[1].get_attribute('src'))
        self.assertTrue("/static/media/teamspace_white_nooutline.4ef184eb.png" in team_space[1].get_attribute('src'))
        time.sleep(1)

        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)
        #
        home_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[1]')[0]
        home_page.click()
        time.sleep(3)
        #
        title = self.browser.find_elements_by_tag_name('img')
        # print(title[2].get_attribute('src'))
        self.assertTrue("/static/media/LaunchNowButton.433790da.png", title[0].get_attribute('src'))
    #
    def test_astronauts_sort(self):
        # print("astro")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        # rockets_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        astronauts_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[2]')[0]
        astronauts_page.click()
        time.sleep(3)

        name_sort = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[2]/div/div/button')[0]
        name_sort.click()
        time.sleep(3)
        name_sort = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[2]/div/div/button')[0]
        name_sort.click()
        time.sleep(3)

        expected_text = "Zhai"
        self.assertTrue(expected_text in self.browser.page_source)

    def test_satellites_sort(self):
        # print("satel")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        # rockets_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        satellites_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[4]')[0]
        satellites_page.click()
        time.sleep(3)

        date_sort = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[3]/div/div/button')[0]
        date_sort.click()
        time.sleep(3)
        date_sort = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[3]/div/div/button')[0]
        date_sort.click()
        time.sleep(3)

        expected_text = "Aerocube 10A"
        self.assertTrue(expected_text in self.browser.page_source)

    def test_countries_search(self):
        # print("count")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        # rockets_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        countries_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[3]')[0]
        countries_page.click()
        time.sleep(1)

        search_bar = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/input')[0]
        search_bar.send_keys("iNdIa")
        time.sleep(2)

        expected_text = "India"
        self.assertTrue(expected_text in self.browser.page_source)

    def test_countries_filter(self):
        # print("count")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        # rockets_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        countries_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[3]')[0]
        countries_page.click()
        time.sleep(1)

        filter = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/div/div[2]/div/table/thead/tr[2]/th[5]/div/input')[0]
        filter.send_keys("5")
        time.sleep(2)

        expected_text = "Algeria"
        self.assertTrue(expected_text in self.browser.page_source)

    def test_countries_filter2(self):
        # print("count")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        # rockets_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        countries_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[3]')[0]
        countries_page.click()
        time.sleep(1)

        filter = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/div/div[2]/div/table/thead/tr[2]/th[5]/div/input')[0]
        filter.send_keys("11")
        time.sleep(2)

        italy = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[2]/span/a')[0]
        italy.click()
        time.sleep(2)
        expected_text = "University of Rome"
        self.assertTrue(expected_text in self.browser.page_source)

    def test_astronauts_sort(self):
        # print("astro")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        # rockets_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        astronauts_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[2]')[0]
        astronauts_page.click()
        time.sleep(3)

        name_sort = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[4]/div/div/button')[0]
        name_sort.click()
        time.sleep(1)
        name_sort = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/div/div[2]/div/table/thead/tr[1]/th[4]/div/div/button')[0]
        name_sort.click()
        time.sleep(3)
        expected_text = "Alan Bean"
        self.assertTrue(expected_text in self.browser.page_source)

    def test_astronauts_filter(self):
        # print("count")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        # rockets_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        astronauts_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[2]')[0]
        astronauts_page.click()
        time.sleep(3)

        filter = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/div/div[2]/div/table/thead/tr[2]/th[2]/div/input')[0]
        filter.send_keys("neil")
        time.sleep(2)

        neil = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/div/div[2]/div/table/tbody/tr/td[2]/span/a/a')[0]
        neil.click()
        time.sleep(2)
        expected_text = "Neil Alden Armstrong"
        self.assertTrue(expected_text in self.browser.page_source)

    def test_astronauts_filter(self):
        # print("count")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        # rockets_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        astronauts_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[2]')[0]
        astronauts_page.click()
        time.sleep(3)

        filter = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/div/div[2]/div/table/thead/tr[2]/th[4]/div/input')[0]
        filter.send_keys("afgh")
        time.sleep(2)

        afghanistan = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/div/div[2]/div/table/tbody/tr/td[1]/span/a/a')[0]
        afghanistan.click()
        time.sleep(2)
        expected_text = "Abdul Ahad Mohmand"
        self.assertTrue(expected_text in self.browser.page_source)

    def test_astronauts_search(self):
        # print("count")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        # rockets_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        astronauts_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[2]')[0]
        astronauts_page.click()
        time.sleep(3)

        search_bar = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/input')[0]
        search_bar.send_keys("Korea")
        time.sleep(2)

        expected_text = "Yi So-yeon"
        self.assertTrue(expected_text in self.browser.page_source)

    def test_satellites_search(self):
        # print("satel")
        nav = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[1]/header/div/button/span[1]')[0]
        nav.click()
        time.sleep(1)

        # rockets_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[5]')[0]
        satellites_page = self.browser.find_elements_by_xpath('/html/body/div[2]/div[3]/div/ul/a[4]')[0]
        satellites_page.click()
        time.sleep(3)

        search_bar = self.browser.find_elements_by_xpath('//*[@id="root"]/div/div[2]/div/div/input')[0]
        search_bar.send_keys("China")
        time.sleep(2)

        expected_text = "Jilin-1"
        self.assertTrue(expected_text in self.browser.page_source)


if __name__ == '__main__':
	main()
