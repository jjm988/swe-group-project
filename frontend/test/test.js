import React from 'react';
import ReactDOM from 'react-dom';
import {shallow, configure, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Drawer from '../src/components/parts/navigation.js';
import About from "../src/components/pages/about";
import Starting from "../src/components/pages/gettingStarted";
import Search from "../src/components/pages/search"
import NoMatch from '../src/components/pages/no_match'
import Splash_Slideshow from '../src/components/parts/slideshow.js'
import Paper from '@material-ui/core/Paper';


//Models
import Astronauts from "../src/components/pages/astronauts/astronauts";
import Countries from "../src/components/pages/countries/countries";
import Satellites from "../src/components/pages/satellites/satellites";

//Instances
import AstronautIP from "../src/components/pages/astronauts/astronaut_IP.js"
import CountryIP from "../src/components/pages/countries/country_IP";
import SatellitesIP from "../src/components/pages/satellites/satellites_IP.js"
import App from '../src/App.js';
import { Route, BrowserRouter } from 'react-router-dom';
import { JSDOM } from 'jsdom';
const { window } = new JSDOM('<!doctype html><html><body></body></html>');

var jsdom = require('jsdom');
// Create a fake DOM for testing with $.ajax
global.window = new jsdom.JSDOM().window;
global.document = window.document;
global.HTMLElement = window.HTMLElement;
import { expect } from 'chai';

configure({ adapter: new Adapter() });

describe('<App />', () => {
	it('renders the App', () => {
	  shallow(<App />);
	});
});

describe('<Astronauts />', () => {
	it('renders Astronauts', () => {
	  shallow(<Astronauts />);
	});
});

describe('<AstronautIP />', () => {
	it('renders Astronauts Instance Page', () => {
	  shallow(<AstronautIP />);
	});
});
describe('<Countries />', () => {
	it('renders Countries', () => {
	  shallow(<Countries />);
	});
});

describe('<CountryIP />', () => {
	it('renders Countries Instance Page', () => {
	  shallow(<CountryIP />);
	});
});

describe('<Satellites />', () => {
	it('renders Satellites', () => {
	  shallow(<Satellites />);
	});
});
describe('<SatellitesIP />', () => {
	it('renders Satellites Instance Page', () => {
	  shallow(<SatellitesIP />);
	});
});

describe('<About />', () => {
	it('renders About page', () => {
	  shallow(<About />);
	});
});

describe('<Starting />', () => {
	it('renders Starting page', () => {
	  shallow(<Starting />);
	});
});

describe('<Search />', () => {
	it('renders Search page', () => {
	  mount(<Search />);
	});
});

describe('<Search >', () => {
  it('should have an input', () => {
    const wrapper = shallow(<Search/>);
    expect(wrapper.find('input')).to.have.length(1);
  });
});

describe('<App />', () => {
	it('renders a Drawer', () => {
	  const wrapper = shallow(<App />);
    expect(wrapper.find(Drawer)).to.have.length(1);

	});
});

describe('<Splash_Slideshow />', () => {
	it('renders a Splash_Slideshow', () => {
	  shallow(<Splash_Slideshow />);
	});
});

describe('<No Match />', () => {
	it('renders No Match', () => {
	  shallow(<NoMatch />);
	});
});

describe('<App />', () => {
  it(' get the correct routes', () => {
      const wrapper = shallow(<App />);
      const correctPath = wrapper.find(Route).reduce((correctPath, route) => {
        const routeProps = route.props();
        correctPath[routeProps.path] = routeProps.component;
        return correctPath;
      }, {});

      expect(correctPath['/astronauts']).to.equal(Astronauts);
      expect(correctPath['/']).to.equal(Splash_Slideshow);
      expect(correctPath['/about']).to.equal(About);
      expect(correctPath['/astronauts/:id']).to.equal(AstronautIP);
      expect(correctPath['/satellites']).to.equal(Satellites);
      expect(correctPath['/satellites/:id']).to.equal(SatellitesIP);
      expect(correctPath['/countries']).to.equal(Countries);
      expect(correctPath['/countries/:id']).to.equal(CountryIP);
    });
  })
