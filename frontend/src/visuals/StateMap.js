import React, { Component } from 'react';
import * as d3 from 'd3';
import stateData from './StateData';
import '../css/StateMap.css';
import axios from 'axios';
import locationVSconcert from './datastore/locationVSconcert.json';

class StateMap extends Component {
	state = {
		status: null,
		states: null
	};

	constructor(props) {
		super(props);
		this.drawChart = this.drawChart.bind(this);
	}

	componentDidMount() {
		this.createData();
	}

	createData() {
		axios.get('https://api.whereyouwannabe.org/num_universities').then((res) => {
			const states = res.data;
			this.setState({ states });
			console.log(states);
			console.log(locationVSconcert);
		});
	}

	tooltipHtml(n, d) {
		/* function to create html content string in tooltip div. */
		return '<h4>' + n + '</h4><table>' + '<tr><td>Concerts: ' + d.low + '</td></tr>' + '</table>';
	}

	drawChart() {
		var states = locationVSconcert;
		console.log(states);
		for (var state in states) {
			if (state == null) {
				states[state] = 0;
			}
		}

		var sampleData = {};
		[
			'HI',
			'AK',
			'FL',
			'SC',
			'GA',
			'AL',
			'NC',
			'TN',
			'RI',
			'CT',
			'MA',
			'ME',
			'NH',
			'VT',
			'NY',
			'NJ',
			'PA',
			'DE',
			'MD',
			'WV',
			'KY',
			'OH',
			'MI',
			'WY',
			'MT',
			'ID',
			'WA',
			'DC',
			'TX',
			'CA',
			'AZ',
			'NV',
			'UT',
			'CO',
			'NM',
			'OR',
			'ND',
			'SD',
			'NE',
			'IA',
			'MS',
			'IN',
			'IL',
			'MN',
			'WI',
			'MO',
			'AR',
			'OK',
			'KS',
			'LA',
			'VA'
		].forEach(function(d) {
			// var p = parkStateDict[d];
			// var o = orgStateDict[d];
			// var e = eventStateDict[d];
			var num = 0;
			if (states[d] != null) {
				num = states[d];
			}
			sampleData[d] = {
				// parks: p, orgs: o, events: e,
				// #73c7f5   11cff5
				color: d3.interpolate('#ffffcc', '#11cff5')(num / 50),
				low: num
			};
		});

		/* draw states on id #statesvg */
		stateData.draw('#statesvg', sampleData, this.tooltipHtml);

		d3.select(window.frameElement).style('height', '600px');
	}

	render() {
		if (this.state.states != null) {
			this.drawChart();
		}
		return (
			<div className="container">
				<div id="tooltip" />
				<svg width="960" height="600" id="statesvg" />
			</div>
		);
	}
}

export default StateMap;
