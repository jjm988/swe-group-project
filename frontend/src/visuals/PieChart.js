import React from 'react';
import * as d3 from 'd3';

class PieChart extends React.Component {
	constructor(props) {
		super(props);
		this.createPieChart = this.createPieChart.bind(this);
	}

	componentDidMount() {
		this.createPieChart();
	}

	createPieChart() {
		const margin = 40;
		const height = 450;
		const width = 1200;
		const radius = Math.min(width, height) / 2 - margin;

		const rawSvg = d3.select(this.refs.pieChart).append('svg').attr('width', width).attr('height', height);
		const svg = rawSvg.append('g').attr('transform', 'translate(' + width / 2.5 + ',' + height / 2 + ')');

		const pie = d3.pie().value((d) => d.value);
		const data = pie(d3.entries(this.props.data).slice(0, 10).sort((a, b) => b.value - a.value));
		const arcGenerator = d3.arc().innerRadius(0).outerRadius(radius);
		const colorScale = d3.scaleOrdinal(d3.schemePaired);

		svg
			.selectAll('mySlices')
			.data(data)
			.enter()
			.append('path')
			.attr('fill', (d, i) => colorScale(i))
			.style('opacity', 0.9)
			.attr('d', arcGenerator)
			.append('text')
			.text((d) => `${d.data.key}: ${d.data.value}`);

		const legend = rawSvg
			.selectAll('legendElems')
			.data(data)
			.enter()
			.append('g')
			.attr('transform', (d, i) => 'translate(' + (width - 450) + ',' + (i * 40 + 20) + ')')
			.attr('class', 'legend');
		legend.append('rect').attr('width', 10).attr('height', 10).attr('fill', (d, i) => colorScale(i));
		legend
			.append('text')
			.text((d) => `${d.data.key}: ${d.data.value}`)
			.style('font-size', 16)
			.style('font-family', 'Arial')
			.style('fill', '#FFFFFF')
			.attr('font-weight', 'bold')
			.attr('y', 10)
			.attr('x', 15);
	}

	render() {
		return <div ref="pieChart" />;
	}
}

export default PieChart;
