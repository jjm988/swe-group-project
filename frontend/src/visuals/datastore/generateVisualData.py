import json, requests
import random

def get_location_concert():
    filename = 'locationVSconcert.json'
    url = 'https://bandtogetherapi.xyz/restapi/concert/search'
    response = requests.get(url)
    jsonFile = response.json()
    locations = {}
    for obj in jsonFile:
        city = obj['region']
        if city in locations:
            locations[city] += 1
        else:
            locations[city] = 1

    with open(filename, 'w') as outfile:
        js = json.dump(locations, outfile)




if __name__ == "__main__":
    get_location_concert()
