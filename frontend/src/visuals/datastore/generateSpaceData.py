import json, requests, datetime, sys, csv
import itertools


def get_astronauts():
    filename = 'astronautsPerCountry.json'
    url = 'https://api.landitinspace.net/api/countries'
    response = requests.get(url)
    jsonFile = response.json()
    output = []

    for country in jsonFile['objects']:
        dict = {}
        if(country['count_astronauts'] != 0):
            dict["label"] = country['country']
            dict["value"] = country['count_astronauts']
            output.append(dict)
    print(output)
    with open(filename, 'w') as outfile:
        js = json.dump(output, outfile)

def get_satellites():
    filename = 'satellitesPerCountry.json'
    url = 'https://api.landitinspace.net/api/satellites'
    url2 = 'https://api.landitinspace.net/api/countries'
    response = requests.get(url)
    response2 = requests.get(url2)
    jsonFile = response.json()
    jsonFile2 = response2.json()
    output = []

    satelliteDict={}
    for satellite in jsonFile['objects']:
        country_name = satellite['country']
        date = satellite['date_of_launch']
        dateArray = date.split("/")
        tup = (country_name, dateArray[-1])
        if tup in satelliteDict:
            satelliteDict[tup]+=1
        else:
            satelliteDict[tup]=1

    for element in satelliteDict:
        dict = {}
        dict['date'] = element[1]
        dict['name'] = element[0]
        dict['category'] = element[0]
        dict['value'] = satelliteDict[element]
        output.append(dict)

    with open(filename, 'w') as outfile:
        js = json.dump(output, outfile)

def get_countries():
    filename = 'countryPurpose.json'
    url = 'https://api.landitinspace.net/api/countries'
    response = requests.get(url)
    jsonFile = response.json()
    dict = {}

    for country in jsonFile['objects']:
        purpose = country['purposes']
        purposeArray = purpose.split(",")
        for p in purposeArray:
            if p in dict:
                dict[p]+=1
            else:
                dict[p]=1

    with open(filename, 'w') as outfile:
        js = json.dump(dict, outfile)

if __name__ == "__main__":
    get_countries()
