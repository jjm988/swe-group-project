import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles((theme) => ({
	root: {
		margin: 'auto',
		minWidth: 280,
		maxWidth: 345,
		alignContent: 'center',
		padding: '20px',
		display: 'flex',
		flexDirection: 'column'
	},
	media: {
		height: 400
	},
	large: {
		width: theme.spacing(30),
		height: theme.spacing(30),
		margin: 'auto'
	},
	card: {
		display: 'flex',
		flexDirection: 'column'
	},
	avatar: {
		width: 60,
		height: 60,
		margin: 'auto'
	}
}));

export default function ToolCard(props) {
	const classes = useStyles();

	return (
		<Card className={classes.root} style={{ background: props.bColor }}>
			<Avatar
				style={useStyles.avatar}
				src={require(`../../images/tools/${props.name}.png`)}
				className={classes.large}
			/>
			<CardContent>
				<br />
				<Typography color="textSecondary" variant="h6" style={{ color: 'orange' }}>
					{props.title}
				</Typography>
				<Typography variant="body1" color="textSecondary" component="p">
					<br />
					{props.info}
				</Typography>
			</CardContent>
		</Card>
	);
}
