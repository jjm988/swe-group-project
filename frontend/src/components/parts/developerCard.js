import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles((theme) => ({
	root: {
		margin: 'auto',
		minWidth: 280,
		maxWidth: 345,
		alignContent: 'center',
		padding: '20px',
		display: 'flex',
		flexDirection: 'column'
	},
	media: {
		height: 400
	},
	large: {
		width: theme.spacing(30),
		height: theme.spacing(30),
		margin: 'auto'
	},
	card: {
		display: 'flex',
		flexDirection: 'column'
	},
	avatar: {
		width: 60,
		height: 60,
		margin: 'auto'
	}
}));

export default function MediaCard(props) {
	const classes = useStyles();

	return (
		<Card className={classes.root}>
			<Avatar
				style={useStyles.avatar}
				src={require(`../../images/team/${props.name}.jpg`)}
				className={classes.large}
			/>
			<CardContent>
				<Typography color="textSecondary" variant="h6" style={{ color: 'red' }}>
					{props.name}
				</Typography>
				<Typography variant="body1" color="textSecondary" component="p">
					<br />
					{props.role}
					<br />
					<br />
					{props.bio}
					<br />
					<br />
					<Typography variant="body1" align="center" style={{ color: 'orange' }}>
						{props.info}
					</Typography>
					<br />
					Commits: {props.commits}
					<br />
					Issues Created: {props.issuesOpened}
					<br />
					Issues Closed: {props.issuesClosed}
					<br />
					Unit Tests: {props.unitTest}
					<br />
					{props.email}
				</Typography>
			</CardContent>
		</Card>
	);
}
