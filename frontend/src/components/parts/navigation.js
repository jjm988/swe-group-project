import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import IconButton from '@material-ui/core/IconButton';
import ListItem from '@material-ui/core/ListItem';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';
import titleimage from '../../images/NASA_fonts/title.png';
import CssBaseline from '@material-ui/core/CssBaseline';
import StarField from '../../images/splash_page/StarField.png';
import ArrowBackIos from '@material-ui/icons/ArrowBackIos';
import Search from '@material-ui/icons/Search';

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
	list: {
		width: 250,
		height: 1000,
		backgroundColor: 'black',
		backgroundImage: `url(${StarField})`
	},
	fullList: {
		width: 'auto'
	},
	appBar: {
		transition: theme.transitions.create([ 'margin', 'width' ], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen
		})
	},
	appBarShift: {
		width: `calc(100% - ${drawerWidth}px)`,
		marginLeft: drawerWidth,
		transition: theme.transitions.create([ 'margin', 'width' ], {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen
		})
	},
	menuButton: {
		color: 'secondary',
		marginRight: theme.spacing(2)
	},
	hide: {
		display: 'none'
	},
	logo: {
		maxWidth: 300,
		alignItems: 'center'
	},
	paper: {
		height: 'calc(100% - 64px)',
		top: 64
	}
}));

export default function TemporaryDrawer() {
	const classes = useStyles();
	const [ state, setState ] = React.useState({
		top: false,
		left: false,
		bottom: false,
		right: false
	});

	const toggleDrawer = (side, open) => (event) => {
		if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
			return;
		}

		setState({ ...state, [side]: open });
	};

	const sideList = (side) => (
		<div
			className={classes.list}
			role="presentation"
			onClick={toggleDrawer(side, false)}
			onKeyDown={toggleDrawer(side, false)}
		>
			<br />
			<br />
			<div className="underline">
				<h2 style={{ textAlign: 'center' }}>
					<img
						src={require(`../../images/NASA_fonts/nav_white.png`)}
						alt="Navigation"
						style={{ maxWidth: 220 }}
					/>
				</h2>
			</div>
			<br />
			<List>
				<ListItem button key="Home" component="a" href="/">
					<img
						src={require(`../../images/NASA_fonts/home_white.png`)}
						alt="Home"
						style={{ maxWidth: 90, margin: 'auto', display: 'flex' }}
					/>
				</ListItem>
				<br />
				<ListItem button key="Astronauts" component="a" href="/astronauts">
					<img
						src={require(`../../images/NASA_fonts/MaterialDark/astronauts_white_nooutline.png`)}
						alt="Astronauts"
						style={{ maxWidth: 170, margin: 'auto', display: 'flex' }}
					/>
				</ListItem>
				<br />
				<ListItem button key="Countries" component="a" href="/countries">
					<img
						src={require(`../../images/NASA_fonts/MaterialDark/countries_white_nooutline.png`)}
						alt="Countries"
						style={{ maxWidth: 140, margin: 'auto', display: 'flex' }}
					/>
				</ListItem>
				<br />
				<ListItem button key="Satellites" component="a" href="/satellites">
					<img
						src={require(`../../images/NASA_fonts/MaterialDark/satellites_white_nooutline.png`)}
						alt="Satellites"
						style={{ maxWidth: 150, margin: 'auto', display: 'flex' }}
					/>
				</ListItem>
				<br />
				<ListItem button key="Visualizations" component="a" href="/visualizations">
					<img
						src={require(`../../images/NASA_fonts/visualizations.png`)}
						alt="Vis"
						style={{ maxWidth: 190, margin: 'auto', display: 'flex' }}
					/>
				</ListItem>
				<br />
				<ListItem button key="About" component="a" href="/about">
					<img
						src={require(`../../images/NASA_fonts/about_white.png`)}
						alt="About us"
						style={{ maxWidth: 80, margin: 'auto', display: 'flex' }}
					/>
				</ListItem>
			</List>
		</div>
	);

	return (
		<div>
			<CssBaseline />
			<AppBar position="fixed">
				<Toolbar>
					<IconButton
						color="inherit"
						aria-label="open drawer"
						onClick={toggleDrawer('left', true)}
						edge="start"
						className={classes.menuButton}
					>
						<MenuIcon />
					</IconButton>
					<IconButton
						color="inherit"
						aria-label="Back"
						component="span"
						onClick={() => {
							window.history.back(-1);
						}}
					>
						<ArrowBackIos />
					</IconButton>
					<a href="/search" style={{ textdecorations: 'none', color: 'inherit' }}>
						<Search />
					</a>
					<a href="/">
						<img src={titleimage} alt="" className={classes.logo} style={{ marginLeft: 5 }} />
					</a>
				</Toolbar>
			</AppBar>
			<Drawer classes={{ paper: classes.paper }} open={state.left} onClose={toggleDrawer('left', false)}>
				{sideList('left')}
			</Drawer>
		</div>
	);
}
