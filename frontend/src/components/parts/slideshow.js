import React from 'react';
import { Fade } from 'react-slideshow-image';
import StarField from '../../images/splash_page/StarField.png';
import GIF from '../../images/outer.gif';
import '../../css/splash.css';
import Grid from '@material-ui/core/Grid';

const fadeProperties = {
	duration: 3500,
	transitionDuration: 500,
	infinite: true,
	indicators: false,
	arrows: false,
	onChange: (oldIndex, newIndex) => {
		console.log(`fade transition from ${oldIndex} to ${newIndex}`);
	}
};

const Splash_Slideshow = () => {
	return (
		<div>
			<Grid
				container
				spacing={0}
				alignItems="center"
				justify="center"
				style={{ minHeight: '100vh', zIndex: '1', position: 'absolute' }}
			>
				<a href="/gettingStarted">
					<img
						src={require(`../../images/splash_page/LaunchNowButton.png`)}
						alt="Navigation"
						style={{ maxWidth: 300 }}
					/>
				</a>
			</Grid>

			<Grid style={{ zIndex: '0', position: 'relative' }}>
				<div className="slide-container">
					<Fade {...fadeProperties}>
						<div className="each-fade">
							<div className="image-container">
								<img src={GIF} id="bg" alt="StarFieldNP" />
							</div>
						</div>
						<div className="each-fade">
							<div className="image-container">
								<img src={StarField} id="bg" alt="StarField" />
							</div>
						</div>
					</Fade>
				</div>
			</Grid>
		</div>
	);
};

export default Splash_Slideshow;
