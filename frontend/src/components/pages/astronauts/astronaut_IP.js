import React, { useEffect } from 'react';
import axios from 'axios';
import '../../../css/modelPage.css';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import Avatar from '@material-ui/core/Avatar';
import background from '../../../images/8kStarField.png';
import ReactLoading from 'react-loading';
import NoMatch from '../no_match';
import { BrowserRouter, Route } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
	root: {
		margin: 'auto',
		minWidth: 280,
		maxWidth: 345,
		alignContent: 'center',
		padding: '20px',
		display: 'flex',
		fontSize: 18,
		flexDirection: 'column',
		backgroundImage: `url(${background})`
	},
	media: {
		height: 400
	},
	large: {
		width: theme.spacing(30),
		height: theme.spacing(30),
		margin: 'auto'
	},
	card: {
		display: 'flex',
		flexDirection: 'column'
	},
	avatar: {
		width: 100,
		height: 100,
		margin: 'auto'
	}
}));

export default function EnhancedTable(props) {
	const [ data, setData ] = React.useState([]);
	const [ done, setDone ] = React.useState(false);
	useEffect(() => {
		axios
			.get(`https://api.landitinspace.net/api/astronauts`)
			.then((res) => {
				const data = res.data.objects;
				var pages = window.location.pathname.split('/');
				var last = decodeURI(pages[pages.length - 1]);
				var i;
				let flag = false;
				for (i = 0; i < data.length; i++) {
					if (data[i].name === last) {
						setData(data[i]);
						flag = true;

						break;
					}
				}
				if (!flag) {
					setDone(true);
				}
			})
			.catch((err) => {
				console.log(err);
			});
	}, []);

	const classes = useStyles();
	var satellite_data = ('' + data.satellites).split(',');
	var wiki_summary = ('' + data.summary).split('https:');

	return (
		<div>
			{data.length == 0 ? (
				<div>
					{!done ? (
						<div className="load">
							<ReactLoading className="Sup" type={'bars'} color={'blue'} />
						</div>
					) : (
						<div>
							<BrowserRouter>
								<Route component={NoMatch} />
							</BrowserRouter>
						</div>
					)}
				</div>
			) : (
				<div style={{ backgroundImage: `url(${background})` }}>
					<br />
					<br />
					<br />
					<br />
					<br />
					<h1 className="underline1" style={{ color: 'orange' }}>
						{' '}
						{data.name}{' '}
					</h1>
					<br />
					<br />

					<Avatar style={useStyles.avatar} alt={data.name} src={data.img_link} className={classes.large} />
					<br />
					<br />

					<p style={{ fontSize: 20, color: 'red' }}>Summary: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{wiki_summary[0]}
					</p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						<a href={'https:' + wiki_summary[1]} rel="noopener noreferrer" target="_blank">
							{' '}
							{'https:' + wiki_summary[1]}{' '}
						</a>
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Country: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						<Link to={`/countries/${data.country}`}>
							<a href={`/countries/${data.country}`}> {data.country} </a>
						</Link>
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Flights: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.flights}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Satellites: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						<Link to={`/satellites/${satellite_data[0]}`}>
							<a href={`/satellites/${satellite_data[0]}`}> {satellite_data[0]} </a>
						</Link>,{' '}
						<Link to={`/satellites/${satellite_data[1]}`}>
							<a href={`/satellites/${satellite_data[1]}`}> {satellite_data[1]} </a>
						</Link>
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Operators: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.operators}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Gender: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.gender}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Purposes: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.purposes}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Total_Flight_Time: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.total_flight_time}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Total_Flights: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.total_flights}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Users: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.users}
					</p>

					<br />
					<br />
					<div className="video-frame w-100" style={{ overflow: 'hidden' }}>
						<iframe
							id="player2"
							width="700"
							height="400"
							src={data.vid_link}
							frameBorder="0"
							title="Astronaut Video"
							allow="autoplay; encrypted-media"
							allowFullScreen
						/>
					</div>
					<br />
					<br />
					<br />
				</div>
			)}
		</div>
	);
}
