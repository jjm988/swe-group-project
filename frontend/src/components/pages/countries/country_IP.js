import React, { useEffect } from 'react';
import axios from 'axios';
import '../../../css/modelPage.css';
import ReactLoading from 'react-loading';
import Link from '@material-ui/core/Link';
import background from '../../../images/8kStarField.png';
import NoMatch from '../no_match';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

export default function EnhancedTable(props) {
	const [ data, setData ] = React.useState([]);
	const [ done, setDone2 ] = React.useState(false);

	useEffect(() => {
		axios
			.get(`https://api.landitinspace.net/api/countries`)
			.then((res) => {
				const data = res.data.objects;
				var pages = window.location.pathname.split('/');
				var last = decodeURI(pages[pages.length - 1]);
				var i;
				var flag = false;
				for (i = 0; i < data.length; i++) {
					if (data[i].country === last) {
						setData(data[i]);
						flag = true;
						break;
					}
				}
				if (!flag) {
					setDone2(true);
				}
			})
			.catch((err) => {
				console.log(err);
			});
	}, []);

	var satellite_data = ('' + data.satellites).split(',');
	var astro_data = ('' + data.astronauts).split(',');
	var wiki_summary = ('' + data.summary).split('https:');

	const baseUrl = 'https://www.google.com/maps/embed/v1/search?zoom=3&key=';
	const googleApiKey = 'AIzaSyB20EOZM-IzQ85Yz6Fxc8Y5cWlXR_Ibj74';
	const formatedAddress = 'satellite launch sites in ' + data.country;
	const sourceUrl = baseUrl + googleApiKey + '&q=' + formatedAddress;
	return (
		<div>
			{data.length == 0 ? (
				<div>
					{!done ? (
						<div className="load">
							<ReactLoading className="Sup" type={'bars'} color={'blue'} />
						</div>
					) : (
						<div>
							<BrowserRouter>
								<Route component={NoMatch} />
							</BrowserRouter>
						</div>
					)}
				</div>
			) : (
				<div style={{ backgroundImage: `url(${background})` }}>
					<br />
					<br />
					<br />
					<br />
					<br />
					<h1 className="underline1" style={{ color: 'orange' }}>
						{' '}
						{data.country}{' '}
					</h1>
					<br />
					<br />

					<img className="square" src={data.img_link} alt={data.country} />
					<br />
					<br />

					<p style={{ fontSize: 20, color: 'red' }}>Summary: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{wiki_summary[0]}
					</p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						<a href={'https:' + wiki_summary[1]} rel="noopener noreferrer" target="_blank">
							{' '}
							{'https:' + wiki_summary[1]}{' '}
						</a>
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Satellites: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						<Link to={`/satellites/${satellite_data[0]}`}>
							<a href={`/satellites/${satellite_data[0]}`}> {satellite_data[0]} </a>
						</Link>,{' '}
						<Link to={`/satellites/${satellite_data[1]}`}>
							<a href={`/satellites/${satellite_data[1]}`}> {satellite_data[1]} </a>
						</Link>
					</p>

					<p style={{ fontSize: 20, color: 'red' }}> Launch Sites: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.launch_sites}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}> Astronauts: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						<Link to={`/astronauts/${astro_data[0]}`}>
							<a href={`/astronauts/${astro_data[0]}`}> {astro_data[0]} </a>
						</Link>,{' '}
						<Link to={`/astronauts/${astro_data[1]}`}>
							<a href={`/astronauts/${astro_data[1]}`}> {astro_data[1]} </a>
						</Link>
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Launch Vehicles: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.launch_vehicles}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>No. of Astronauts: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.count_astronauts}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>No. of Satellites: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.count_satellites}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Operators: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.operators}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Purposes: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.purposes}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Users: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.users}
					</p>

					<br />
					<br />
					<div class="video-frame" style={{ overflow: 'hidden' }}>
						<iframe
							id="country"
							width="800"
							height="500"
							style={{ border: 0 }}
							frameborder="0"
							title="Country Map"
							allowfullscreen
							src={sourceUrl}
						/>
					</div>
					<br />
					<br />
					<br />
				</div>
			)}
		</div>
	);
}
