import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import astronautsPerCountry from '../../visuals/datastore/astronautsPerCountry.json';
import satellitesPerCountry from '../../visuals/datastore/satellitesPerCountry.json';
import followerVsPopularityScore from '../../visuals/datastore/followerVsPopularityScore.json';
import genreVSartists from '../../visuals/datastore/genreVSartists.json';
import countryPurpose from '../../visuals/datastore/countryPurpose.json';
import locationVSconcert from '../../visuals/datastore/locationVSconcert.json';
import Scatterplot from '../../visuals/Scatterplot.js';
import BarChart from '../../visuals/barChart.js';
import PieChart from '../../visuals/PieChart.js';
import StateMap from '../../visuals/StateMap.js';
import BubbleChart from '@weknow/react-bubble-chart-d3';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

function TabPanel(props) {
	const { children, value, index, ...other } = props;

	return (
		<Typography
			component="div"
			role="tabpanel"
			hidden={value !== index}
			id={`simple-tabpanel-${index}`}
			aria-labelledby={`simple-tab-${index}`}
			{...other}
		>
			{value === index && <Box p={3}>{children}</Box>}
		</Typography>
	);
}
TabPanel.propTypes = {
	children: PropTypes.node,
	index: PropTypes.any.isRequired,
	value: PropTypes.any.isRequired
};

function a11yProps(index) {
	return {
		id: `simple-tab-${index}`,
		'aria-controls': `simple-tabpanel-${index}`
	};
}

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
		backgroundColor: theme.palette.background.paper
	}
}));

function Visualizations(props) {
	const [ iteration, setIteration ] = useState(0);
	const [ start, setStart ] = useState(false);
	const [ data, setData ] = useState([
		{
			name: 'alpha',
			value: 10,
			color: '#f4efd3'
		},
		{
			name: 'beta',
			value: 15,
			color: '#cccccc'
		},
		{
			name: 'charlie',
			value: 20,
			color: '#c2b0c9'
		},
		{
			name: 'delta',
			value: 25,
			color: '#9656a1'
		},
		{
			name: 'echo',
			value: 30,
			color: '#fa697c'
		},
		{
			name: 'foxtrot',
			value: 35,
			color: '#fcc169'
		}
	]);
	const classes = useStyles();
	const [ value, setValue ] = React.useState(0);
	const handleChange = (event, newValue) => {
		setValue(newValue);
	};

	return (
		<div style={{ textAlign: 'center', paddingTop: '3em', paddingLeft: '10em', paddingRight: '10em' }}>
			<br />
			<br />
			<br />
			<img
				src={require(`../../images/NASA_fonts/visualizations.png`)}
				alt="Satellites"
				style={{ maxWidth: 600, boxShadow: '5px 5px 30px -10px blue' }}
			/>
			<br />
			<br />
			<br />
			<br />
			<br />
			<div className={classes.root}>
				<AppBar position="static">
					<Tabs variant="fullWidth" value={value} onChange={handleChange} aria-label="simple tabs example">
						<Tab label="Astronauts" {...a11yProps(0)} />
						<Tab label="Satellites" {...a11yProps(1)} />
						<Tab label="Countries" {...a11yProps(2)} />
						<Tab label="Provider's Visualizations" {...a11yProps(3)} />
					</Tabs>
				</AppBar>
				<TabPanel value={value} index={0}>
					<br />
					<h2>Astronauts Per Country</h2>
					<BubbleChart
						graph={{
							zoom: 0.75,
							offsetX: 0.15,
							offsetY: -0.01
						}}
						data={astronautsPerCountry}
						width={1100}
						height={900}
						padding={15} // optional value, number that set the padding between bubbles
						showLegend={true} // optional value, pass false to disable the legend.
						legendPercentage={20} // number that represent the % of with that legend going to use.
						legendFont={{
							family: 'Arial',
							size: 16,
							color: '#FFFFFF',
							weight: 'bold'
						}}
						valueFont={{
							family: 'Arial',
							size: 12,
							color: '#fff',
							weight: 'bold'
						}}
						labelFont={{
							family: 'Arial',
							size: 16,
							color: '#fff',
							weight: 'bold'
						}}
					/>
				</TabPanel>
				<TabPanel value={value} index={1}>
					<br />
					<h2>Satellites Per Country</h2>
					<BarChart data={satellitesPerCountry.slice(0, 100)} xAttr="name" yAttr="value" />
				</TabPanel>
				<TabPanel value={value} index={2}>
					<br />
					<h2>Purposes Per Country</h2>
					<PieChart data={countryPurpose} />
				</TabPanel>
				<TabPanel value={value} index={3}>
					<br />
					<h2>Number of Concerts Per State</h2>
					<br />
          <PieChart
            data={locationVSconcert}
          />
					<br />
					<br />
					<br />
					<h2>Number of Artists Per Genre</h2>
					<br />
					<br />
					<BubbleChart
						graph={{
							zoom: 0.75,
							offsetX: 0.15,
							offsetY: -0.01
						}}
						data={genreVSartists}
						width={1100}
						height={800}
						padding={15} // optional value, number that set the padding between bubbles
						showLegend={true} // optional value, pass false to disable the legend.
						legendPercentage={20} // number that represent the % of with that legend going to use.
						legendFont={{
							family: 'Arial',
							size: 16,
							color: '#FFFFFF',
							weight: 'bold'
						}}
						valueFont={{
							family: 'Arial',
							size: 12,
							color: '#fff',
							weight: 'bold'
						}}
						labelFont={{
							family: 'Arial',
							size: 16,
							color: '#fff',
							weight: 'bold'
						}}
					/>
					<h2>Number of Spotify Followers vs Popularity score</h2>
					<Scatterplot
						data={followerVsPopularityScore}
						xAttr="follower"
						yAttr="popularity"
						xMax={4000000}
						yMax={100}
						xLabel="Number of Followers"
						yLabel="Popularity Score"
						style={{ color: 'white' }}
					/>
				</TabPanel>
			</div>

			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
		</div>
	);
}

export default withRouter(Visualizations);
