import React, { useEffect } from 'react';
import '../../css/about.css';
import { makeStyles } from '@material-ui/core/styles';
import DevCard from '../parts/developerCard.js';
import ToolCard from '../parts/toolCard.js';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
	root: {
		marginTop: '75px',
		flexGrow: 1
	},
	mediacard: {
		height: 140,
		width: 130
	}
}));

//---------------------
//---Get Gitlab Data---
//---------------------

// User Email key
var userEmailName = new Map();
userEmailName
	.set('jiamingji988@gmail.com', 'Jiaming Ji')
	.set('smrutishah1998@gmail.com', 'Smruti Shah')
	.set('evanhodde20@gmail.com', 'Evan Hodde')
	.set('namit.agrawal20@gmail.com', 'Namit Agrawal')
	.set('ckulkarni5@utexas.edu', 'Chinmayee Kulkarni')
	.set('chinmayee5sk@gmail.com', 'Chinmayee Kulkarni')
	.set('sonalibhat@utexas.edu', 'Sonali Bhat')
	.set('namitagrawal@wireless-10-147-242-133.public.utexas.edu', 'Namit Agrawal')
	.set('namit.agrawal20@gmail.comgit config user.name Namit', 'Namit Agrawal');

export default function About() {
	const [ Namit, setNamit ] = React.useState(0);
	const [ Evan, setEvan ] = React.useState(0);
	const [ Smruti, setSmruti ] = React.useState(0);
	const [ Jiaming, setJiaming ] = React.useState(0);
	const [ Chinmayee, setChinmayee ] = React.useState(0);
	const [ Sonali, setSonali ] = React.useState(0);
	const [ NamitOpen, setNamitO ] = React.useState(0);
	const [ EvanOpen, setEvanO ] = React.useState(0);
	const [ SmrutiOpen, setSmrutiO ] = React.useState(0);
	const [ JiamingOpen, setJiamingO ] = React.useState(0);
	const [ ChinmayeeOpen, setChinmayeeO ] = React.useState(0);
	const [ SonaliOpen, setSonaliO ] = React.useState(0);
	const [ NamitClosed, setNamitC ] = React.useState(0);
	const [ EvanClosed, setEvanC ] = React.useState(0);
	const [ SmrutiClosed, setSmrutiC ] = React.useState(0);
	const [ JiamingClosed, setJiamingC ] = React.useState(0);
	const [ ChinmayeeClosed, setChinmayeeC ] = React.useState(0);
	const [ SonaliClosed, setSonaliC ] = React.useState(0);
	const [ total_commits, setCommits ] = React.useState(0);
	const [ total_issues, setIssues ] = React.useState(0);

	const classes = useStyles();

	useEffect(() => {
		axios
			.get(`https://gitlab.com/api/v4/projects/16896024/repository/contributors`)
			.then((res) => {
				for (const person in res.data) {
					const cur = res.data[person];
					var user = userEmailName.get(cur.email);
					if (user === 'Namit Agrawal') {
						setNamit((prevState) => prevState + cur.commits);
					} else if (user === 'Evan Hodde') {
						setEvan((prevState) => prevState + cur.commits);
					} else if (user === 'Jiaming Ji') {
						setJiaming((prevState) => prevState + cur.commits);
					} else if (user === 'Sonali Bhat') {
						setSonali((prevState) => prevState + cur.commits);
					} else if (user === 'Chinmayee Kulkarni') {
						setChinmayee((prevState) => prevState + cur.commits);
					} else if (user === 'Smruti Shah') {
						setSmruti((prevState) => prevState + cur.commits);
					}
					setCommits((prevState) => prevState + cur.commits);
				}
			})
			.catch((err) => console.log(err));
		var usernames = [
			'?author_username=namit.agrawal20',
			'?author_username=ckulkarni5',
			'?author_username=evanhodde20',
			'?author_username=jjm988',
			'?author_username=shahsmruti',
			'?author_username=sonali17',
			''
		];
		for (let name of usernames)
			axios
				.get(`https://gitlab.com/api/v4/projects/16896024/issues_statistics` + name)
				.then((res) => {
					if (name === '?author_username=namit.agrawal20') {
						setNamitO(res.data.statistics.counts.all);
						setNamitC(res.data.statistics.counts.closed);
					} else if (name === '?author_username=evanhodde20') {
						setEvanO(res.data.statistics.counts.all);
						setEvanC(res.data.statistics.counts.closed);
					} else if (name === '?author_username=jjm988') {
						setJiamingO(res.data.statistics.counts.all);
						setJiamingC(res.data.statistics.counts.closed);
					} else if (name === '?author_username=sonali17') {
						setSonaliO(res.data.statistics.counts.all);
						setSonaliC(res.data.statistics.counts.closed);
					} else if (name === '?author_username=ckulkarni5') {
						setChinmayeeO(res.data.statistics.counts.all);
						setChinmayeeC(res.data.statistics.counts.closed);
					} else if (name === '?author_username=shahsmruti') {
						setSmrutiO(res.data.statistics.counts.all);
						setSmrutiC(res.data.statistics.counts.closed);
					} else {
						setIssues(res.data.statistics.counts.all + 1);
					}
				})
				.catch((err) => console.log(err));
	}, []);

	return (
		<React.Fragment>
			<Container>
				<div className={classes.heroContent}>
					<Container maxWidth="sm">
						<br />
						<br />
						<br />
						<br />
						<Typography component="h6" variant="h2" align="center" color="textPrimary" gutterBottom>
							<img
								src={require(`../../images/NASA_fonts/MaterialDark/teamspace_white_nooutline.png`)}
								alt="Team Space"
								style={{ maxWidth: 500 }}
							/>
						</Typography>
						<Typography variant="h6" align="center" style={{ color: 'orange' }} paragraph>
							We are a group of computer science students at the University of Texas at Austin passionate
							about Space. For our project in the Software Engineering course (Spring 2020), we were
							inspired to look towards the final frontier by the wonders of space and the rapid modern day
							advances made with companies such as SpaceX leading the new Space Race.
						</Typography>
					</Container>
					<Grid container className={classes.root} spacing={2}>
						<Grid item xs={12}>
							<Typography component="h6" variant="h2" align="center" color="white" gutterBottom>
								<img
									src={require(`../../images/NASA_fonts/MaterialDark/theteam_white_nooutline.png`)}
									alt="The Team"
									style={{ maxWidth: 500, alignItems: 'left' }}
								/>
							</Typography>
							<Container className={classes.cardGrid} maxWidth="md">
								<Grid container spacing={4}>
									<Grid item xs={12} sm={6} md={4}>
										<DevCard
											className={classes.mediacard}
											bio="Programmer, Photographer, Cyclist, Pianist"
											info="Worked on Filtering Implementation, Acceptance Tests, AWS, Backend / Frontend Servers, Docker, Adding Data to Instance Pages, and Resolving Customer User Stories
"
											role="Full-Stack Developer"
											issuesClosed={JiamingClosed}
											issuesOpened={JiamingOpen}
											commits={Jiaming}
											name="Jiaming"
											unitTest={8}
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<DevCard
											className={classes.mediacard}
											bio="Programmer, Avid Cricketer, NBA Fan"
											info="Worked on Searching and Sorting Implementations, Frontend Tests, Creating and Refining Model Pages, Redirecting irrelevant page to 404 Error, and Resolving Customer User Stories"
											role="Frontend Developer"
											issuesClosed={NamitClosed}
											issuesOpened={NamitOpen}
											commits={Namit}
											name="Namit"
											unitTest={13}
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<DevCard
											className={classes.mediacard}
											bio="Programmer, Product Manager, Designer, Flautist"
											info="Worked on Searching Implementation, Designing and Testing Postman API, Backend Tests, Technical Report, UML Diagram, and Creating User Stories for Developer Team"
											role="Backend Developer"
											issuesClosed={ChinmayeeClosed}
											issuesOpened={ChinmayeeOpen}
											commits={Chinmayee}
											name="Chinmayee"
											unitTest={25}
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<DevCard
											className={classes.mediacard}
											bio="Programmer, Artist, Designer, Ukulelist"
											info="Worked on Sorting and Filtering Implementation, Designing and Testing Postman API, Backend Tests, Entire Website Graphics, and Resolving Customer User Stories"
											role="Backend Developer"
											issuesClosed={SonaliClosed}
											issuesOpened={SonaliOpen}
											commits={Sonali}
											name="Sonali"
											unitTest={25}
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<DevCard
											className={classes.mediacard}
											bio="Programmer, Commit Spammer, IT Specialist"
											info="Worked on Searching Implementation, Creating and Refining Model Pages, Frontend Tests, Creating User Stories for Developer Team, Resolving Customer User Stories
"
											role="Frontend Developer"
											issuesClosed={EvanClosed}
											issuesOpened={EvanOpen}
											commits={Evan}
											name="Evan"
											unitTest={2}
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<DevCard
											className={classes.mediacard}
											bio="Programmer, Coffee Lover, Cheerleader, Resourcer"
											info="Worked on Filtering Implementation, Acceptance Tests, AWS, Adding Data to Instance Pages, Creating and Fixing Pipelines, and Resolving Customer User Stories"
											role="Full-Stack Developer"
											issuesClosed={SmrutiClosed}
											issuesOpened={SmrutiOpen}
											commits={Smruti}
											name="Smruti"
											unitTest={10}
										/>
									</Grid>
								</Grid>
								<br />
							</Container>
						</Grid>
					</Grid>
					<Container maxWidth="sm">
						<br />
						<br />
						<br />
						<Typography component="h6" variant="h2" align="center" color="textPrimary" gutterBottom>
							<img
								src={require(`../../images/NASA_fonts/MaterialDark/repostats_white_nooutline.png`)}
								alt="Repository Statistics"
								style={{ maxWidth: 500, alignItems: 'left' }}
							/>
						</Typography>
						<Typography component="h6" variant="h6" align="center" style={{ color: 'orange' }} gutterBottom>
							{total_commits} Commits
							<br />
							{total_issues} Issues
							<br />
							{83} Unit Test
							<br />
						</Typography>
						<br />
						<Typography component="h6" variant="h2" align="center" color="textPrimary" gutterBottom>
							<img
								src={require(`../../images/NASA_fonts/MaterialDark/datasources_white_nooutline.png`)}
								alt="Data Sources and APIs"
								style={{ maxWidth: 500, alignItems: 'left' }}
							/>
						</Typography>
						<Typography component="h6" variant="h6" align="center" style={{ color: 'red' }} gutterBottom>
							Countries: <br />
							<Typography
								component="h6"
								variant="h6"
								align="center"
								style={{ color: 'white' }}
								gutterBottom
							>
								The countries data source, which was originally in a text file format, was scraped for
								relevant data (our chosen attributes) using a Python script. The results were then
								formatted and converted into a JSON format.
							</Typography>
							<a href="https://www.ucsusa.org/sites/default/files/2019-12/UCS-Satellite-Database-10-1-19.txt">
								{' '}
								https://www.ucsusa.org{' '}
							</a>
							<br />
							<br />
							Satellites: <br />
							<Typography
								component="h6"
								variant="h6"
								align="center"
								style={{ color: 'white' }}
								gutterBottom
							>
								Both the Satellites and Astronauts data sources were already in XLS format, so our data
								was already formatted and did not need to be scraped. We only had to input it into
								MySQL.
							</Typography>
							<a href="https://www.ucsusa.org/resources/satellite-database">
								{' '}
								https://www.ucsusa.org/resources/satellite-database{' '}
							</a>
							<br />
							<br />
							Astronauts: <br />{' '}
							<a href="https://aerospace.csis.org/data/international-astronaut-database/">
								{' '}
								https://aerospace.csis.org/data/international-astronaut-database/{' '}
							</a>
							<br />
							<br />
							Launched vehicles: <br />{' '}
							<a href="https://spacefund.com/launch-database/">
								{' '}
								https://spacefund.com/launch-database/{' '}
							</a>
							<br />
							<br />
							<Typography
								component="h6"
								variant="h6"
								align="center"
								style={{ color: 'white' }}
								gutterBottom
							>
								For all of the images, we used the Azure Image Search API.
								<br />
								For all of the videos, we used the PyPl YouTube Search Package API.
								<br />
								For all of the maps, we used the Google Cloud to get the Maps Embed API.
								<br />
								For all of the summaries, we used the PyPl Wikipedia API.
								<br />
								<br />
							</Typography>
							<Typography
								component="h6"
								variant="h6"
								align="center"
								style={{ color: 'red' }}
								gutterBottom
							>
								Interesting Result of Integrating Disparate Data:
							</Typography>
							<Typography
								component="h6"
								variant="h6"
								align="center"
								style={{ color: 'white' }}
								gutterBottom
							>
								We learned the importance of data standardization through integrating disparate data.
								There are so many ways to refer to the same entity. For example United States of
								America, United States, US, USA, U.S., U.S.A. among others. This was also the case for
								Astronaut names. For example, Neil Armstrong, Neil Alden Armstrong, Neil A. Armstrong,
								N. Armstrong among others. For these types of integration issues, we had to come up with
								a consistent system otherwise we still would have had many unpopulated instance pages
								hiding in our website. Additionally, we have scraped more information about attributes
								on any given model page by utilizing the properties of foreign keys.
								<br />
							</Typography>
						</Typography>
					</Container>
					<Grid container className={classes.root} spacing={2}>
						<Grid item xs={12}>
							<Typography component="h6" variant="h2" align="center" color="textPrimary" gutterBottom>
								<img
									src={require(`../../images/NASA_fonts/MaterialDark/tools_white_nooutline.png`)}
									alt="Tools we used"
									style={{ maxWidth: 400 }}
								/>
							</Typography>
							<Container className={classes.cardGrid} maxWidth="md">
								<Grid container spacing={4}>
									<Grid item xs={12} sm={6} md={4}>
										<ToolCard
											className={classes.mediacard}
											name="aws"
											title="Amazon Web Services"
											info="Used to host website backend and frontend."
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<ToolCard
											className={classes.mediacard}
											name="gitlab"
											title="GitLab"
											info="Used to store files for team collaboration."
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<ToolCard
											className={classes.mediacard}
											name="flask"
											title="Flask"
											info="Used to create backend server/API."
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<ToolCard
											className={classes.mediacard}
											name="react"
											title="React"
											info="Used to create the front-end."
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<ToolCard
											className={classes.mediacard}
											name="postman"
											title="Postman"
											info="Used to design API."
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<ToolCard
											className={classes.mediacard}
											name="python"
											title="Python"
											info="Used to create our server."
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<ToolCard
											className={classes.mediacard}
											name="selenium"
											title="Selenium"
											info="Used to test UI elements interactivity."
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<ToolCard
											className={classes.mediacard}
											name="unittest"
											title="unittest"
											info="Used to test individual functions in server."
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<ToolCard
											className={classes.mediacard}
											name="sqlalchemy"
											title="SQLAlchemy"
											info="Used as the ORM to query the DB in the backend."
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<ToolCard
											className={classes.mediacard}
											name="docker"
											title="Docker"
											info="To create Docker images for frontend and backend."
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<ToolCard
											className={classes.mediacard}
											name="mocha"
											title="Mocha"
											info="Used with chai to test JavaScript files."
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<ToolCard
											className={classes.mediacard}
											name="uml"
											title="PlantUML"
											info="Used to prepare UML diagrams."
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<ToolCard
											className={classes.mediacard}
											name="d3"
											title="D3"
											info="For producing interactive data visualizations in web browsers."
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<ToolCard
											className={classes.mediacard}
											name="sql"
											title="MySQL Workbench (Optional)"
											info="Used for database purposes and to build it."
										/>
									</Grid>
									<Grid item xs={12} sm={6} md={4}>
										<ToolCard
											className={classes.mediacard}
											name="azure"
											title="Microsoft Azure (Optional)"
											info="Used to scrape and get the image APIs."
										/>
									</Grid>
								</Grid>
								<br />
							</Container>
						</Grid>
					</Grid>
					<div className={classes.heroButtons}>
						<br />
						<br />
						<Grid container spacing={2} justify="center">
							<Grid item>
								<Button
									variant="outlined"
									color="secondary"
									style={{ color: 'white' }}
									href="https://gitlab.com/jjm988/swe-group-project"
								>
									Gitlab
								</Button>
							</Grid>
							<Grid item>
								<Button
									variant="outlined"
									color="secondary"
									style={{ color: 'white' }}
									href="https://documenter.getpostman.com/view/10501113/SzKWuxec?version=latest"
								>
									Postman
								</Button>
							</Grid>
							<Grid item>
								<Button
									variant="outlined"
									color="secondary"
									style={{ color: 'white' }}
									href="https://drive.google.com/file/d/1-nt0X0yq0V_Mbw22nMbmgdGax37M_Uci/view?usp=sharing"
								>
									Presentation
								</Button>
							</Grid>
						</Grid>
						<br />
						<br />
					</div>
				</div>
			</Container>
		</React.Fragment>
	);
}
