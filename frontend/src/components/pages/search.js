import React, { useEffect } from 'react';
import axios from 'axios';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIos from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import Badge from '@material-ui/core/Badge';

export default function Search() {
	const [ astronautResult, setAstroResult ] = React.useState([]);
	const [ countriesResult, setCountryResult ] = React.useState([]);
	const [ satellitesResult, setSatellitesResult ] = React.useState([]);
	const [ search, setSearch ] = React.useState('');
	const [ pageAstro, setPageAstro ] = React.useState(0);
	const [ pageCountry, setPageCountry ] = React.useState(0);
	const [ pageSatellite, setPageSatellite ] = React.useState(0);
	const [ showAstro, setShowAstro ] = React.useState(false);
	const [ showCountry, setShowCountry ] = React.useState(false);
	const [ showSatellite, setShowSatellite ] = React.useState(false);
	useEffect(() => {
		axios
			.get(`https://api.landitinspace.net/api/astronauts`)
			.then((res) => {
				const astro = res.data;
				setAstroResult((prevState) => prevState.concat(astro.objects));
			})
			.catch((err) => console.log(err));
		axios
			.get(`https://api.landitinspace.net/api/countries`)
			.then((res) => {
				const country = res.data;
				setCountryResult((prevState) => prevState.concat(country.objects));
			})
			.catch((err) => console.log(err));
		axios
			.get(`https://api.landitinspace.net/api/satellites`)
			.then((res) => {
				const satellite = res.data;
				setSatellitesResult((prevState) => prevState.concat(satellite.objects));
			})
			.catch((err) => console.log(err));
	}, []);

	let astronautSearchMatches = {};
	let countriesSearchMatches = {};
	let satellitesSearchMatches = {};

	if (search.length !== 0) {
		var res = search.split(' ');
		res.push(search);
		for (var astronaut of astronautResult) {
			var attributes = [];
			var flag = false;
			for (var searchTerm of res) {
				if (astronaut.country.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(astronaut.country);
					flag = true;
				}
				if (astronaut.flights.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(astronaut.flights);
					flag = true;
				}
				if (astronaut.gender.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(astronaut.gender);
					flag = true;
				}
				if (astronaut.name.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					flag = true;
				}
				if (astronaut.operators.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(astronaut.operators);
					flag = true;
				}
				if (astronaut.purposes.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(astronaut.purposes);
					flag = true;
				}
				if (astronaut.satellites.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(astronaut.satellites);
					flag = true;
				}
				if (astronaut.total_flight_time.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(astronaut.total_flight_time);
					flag = true;
				}
				if (astronaut.total_flights.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(astronaut.total_flights);
					flag = true;
				}
				if (astronaut.users.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(astronaut.users);
					flag = true;
				}
			}
			if (flag === true) {
				attributes.push(' Summary: ' + astronaut.summary.substring(0, 300) + '...');
				attributes.push(astronaut.img_link);
				astronautSearchMatches[astronaut.name] = attributes;
			}
		}
	}

	if (search.length !== 0) {
		res = search.split(' ');
		res.push(search);
		for (var country of countriesResult) {
			attributes = [];
			flag = false;
			for (searchTerm of res) {
				if (country.astronauts.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(country.astronauts);
					flag = true;
				}
				if (country.count_astronauts.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(country.count_astronauts);
					flag = true;
				}
				if (country.count_satellites.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(country.count_satellites);
					flag = true;
				}
				if (country.country.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(country.country);
					flag = true;
				}
				if (country.launch_sites.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(country.launch_sites);
					flag = true;
				}
				if (country.launch_vehicles.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(country.launch_vehicles);
					flag = true;
				}
				if (country.operators.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(country.operators);
					flag = true;
				}
				if (country.purposes.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(country.purposes);
					flag = true;
				}
				if (country.satellites.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(country.satellites);
					flag = true;
				}
				if (country.users.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(country.users);
					flag = true;
				}
			}
			if (flag === true) {
				attributes.push(' Summary: ' + country.summary.substring(0, 300) + '...');
				attributes.push(country.img_link);
				countriesSearchMatches[country.country] = attributes;
			}
		}
	}

	if (search.length !== 0) {
		res = search.split(' ');
		res.push(search);
		for (var satellite of satellitesResult) {
			attributes = [];
			flag = false;
			for (searchTerm of res) {
				if (satellite.apogee_km.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(satellite.apogee_km);
					flag = true;
				}
				if (satellite.astronauts.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(satellite.astronauts);
					flag = true;
				}
				if (satellite.class_of_orbit.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(satellite.class_of_orbit);
					flag = true;
				}
				if (satellite.country.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(satellite.country);
					flag = true;
				}
				if (satellite.date_of_launch.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(satellite.date_of_launch);
					flag = true;
				}
				if (satellite.expected_lifetime_yr.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(satellite.expected_lifetime_yr);
					flag = true;
				}
				if (satellite.launch_mass_kg.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(satellite.launch_mass_kg);
					flag = true;
				}
				if (satellite.launch_site.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(satellite.launch_site);
					flag = true;
				}
				if (satellite.launch_vehicle.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(satellite.launch_vehicle);
					flag = true;
				}
				if (satellite.longitude_degree.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(satellite.longitude_degree);
					flag = true;
				}
				if (satellite.operator.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(satellite.operator);
					flag = true;
				}
				if (satellite.perigee_km.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(satellite.perigee_km);
					flag = true;
				}
				if (satellite.period_minute.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(satellite.period_minute);
					flag = true;
				}
				if (satellite.purpose.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(satellite.purpose);
					flag = true;
				}
				if (satellite.sat_id.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(satellite.sat_id);
					flag = true;
				}
				if (satellite.satellite.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(satellite.satellite);
					flag = true;
				}
				if (satellite.users.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
					attributes.push(satellite.users);
					flag = true;
				}
			}
			if (flag === true) {
				// update once summary is available
				attributes.push(satellite.operator + ' ' + satellite.purpose + '...');
				attributes.push(satellite.img_link);
				satellitesSearchMatches[satellite.satellite] = attributes;
			}
		}
	}

	function getHighlightedText(text, highlight) {
		// Split on highlight term and include term into parts, ignore case
		const keywordsString = highlight.toLowerCase();

		const keywords = keywordsString.split(' ');

		// equivalent to: /(cake|pie|cookies)/g
		const pattern = new RegExp(`(${keywords.join('|')})`, 'gi');
		const parts = text.split(pattern);

		return (
			<span>
				{' '}
				{parts.map((part, i) => (
					<span key={i} style={keywords.includes(part.toLowerCase()) ? { backgroundColor: 'yellow' } : {}}>
						{part}
					</span>
				))}{' '}
			</span>
		);
	}
	function renderSearchResults(modelSearchResults, modelPage, modelShow, model) {
		return (
			<div>
				{Object.entries(modelSearchResults).slice(modelPage * 5, modelPage * 5 + 5).map(([ key, value ]) => {
					return (
						<div>
							{modelShow ? (
								<Container
									style={{
										display: 'flex',
										paddingBottom: '2em',
										paddingTop: '2em',
										backgroundColor: '#ebf3fa'
									}}
								>
									<img
										style={{ maxHeight: 125, maxWidth: 125, minWidth: 125, paddingRight: '1em' }}
										title={key}
										alt={key}
										src={value.pop()}
									/>
									<div>
										<h4 style={{ color: 'black', textAlign: 'left' }}>
											<a href={`/${model}/${key}`}>{getHighlightedText(key, search)}</a>
										</h4>
										<h6 style={{ color: 'black', textAlign: 'left' }}>
											{getHighlightedText(value.toString(), search)}
										</h6>
									</div>
									<br />
									<br />
								</Container>
							) : null}
						</div>
					);
				})}
				{modelShow ? (
					<div style={{ display: 'flex', justifyContent: 'flex-end', paddingRight: '5em' }}>
						<IconButton color="inherit" aria-label="Back" component="span" onClick={goBackwardAstro}>
							<ArrowBackIos />
						</IconButton>
						<p style={{ paddingTop: '1em', color: 'white' }}>
							{pageAstro + 1}/{Math.ceil(Object.keys(astronautSearchMatches).length / 5)}
						</p>
						<IconButton color="inherit" aria-label="Front" component="span" onClick={goForwardAstro}>
							<ArrowForwardIosIcon />
						</IconButton>
					</div>
				) : null}
			</div>
		);
	}

	const goBackwardAstro = (event) => {
		if (pageAstro > 0) {
			setPageAstro(pageAstro - 1);
		}
	};
	const goForwardAstro = (event) => {
		if (pageAstro < Math.ceil(Object.keys(astronautSearchMatches).length / 5) - 1) {
			setPageAstro(pageAstro + 1);
		}
	};
	const goBackwardCountry = (event) => {
		if (pageCountry > 0) {
			setPageCountry(pageCountry - 1);
		}
	};
	const goForwardCountry = (event) => {
		if (pageCountry < Math.ceil(Object.keys(countriesSearchMatches).length / 5) - 1) {
			setPageCountry(pageCountry + 1);
		}
	};
	const goBackwardSatellite = (event) => {
		if (pageSatellite > 0) {
			setPageSatellite(pageSatellite - 1);
		}
	};
	const goForwardSatellite = (event) => {
		if (pageSatellite < Math.ceil(Object.keys(satellitesSearchMatches).length / 5) - 1) {
			setPageSatellite(pageSatellite + 1);
		}
	};
	const toggleAstro = (event) => {
		document.getElementById('Astronauts').style.border = '10px solid #121682';
		document.getElementById('Countries').style.border = '10px solid #FFFFFF';
		document.getElementById('Satellites').style.border = '10px solid #FFFFFF';
		setShowAstro(!showAstro);
		setShowCountry(false);
		setShowSatellite(false);
	};
	const toggleCountry = (event) => {
		document.getElementById('Countries').style.border = '10px solid #121682';
		document.getElementById('Astronauts').style.border = '10px solid #FFFFFF';
		document.getElementById('Satellites').style.border = '10px solid #FFFFFF';
		setShowCountry(!showCountry);
		setShowAstro(false);
		setShowSatellite(false);
	};
	const toggleSatellite = (event) => {
		document.getElementById('Satellites').style.border = '10px solid #121682';
		document.getElementById('Astronauts').style.border = '10px solid #FFFFFF';
		document.getElementById('Countries').style.border = '10px solid #FFFFFF';
		setShowSatellite(!showSatellite);
		setShowCountry(false);
		setShowAstro(false);
	};

	return (
		<div>
			<br />
			<br />
			<br />
			<br />
			<br />
			<div style={{ display: 'flex', justifyContent: 'space-around', height: 50 }}>
				<Grid container justify="center">
					<Grid item xs={12} sm={6} md={4}>
						<input
							style={{ fontSize: 25, paddingLeft: 10 }}
							className="SearchBar"
							type="text"
							onChange={(event) => setSearch(event.target.value)}
							placeholder="Search"
						/>
					</Grid>
				</Grid>
			</div>
			<div />
			<br />
			<br />
			<Grid container spacing={2} justify="center">
				<Grid item xs={12} sm={6} md={4}>
					<Badge max={999} badgeContent={Object.keys(astronautSearchMatches).length} color="primary">
						<img
							src={require(`../../images/NASA_fonts/MaterialDark/astronauts_white_nooutline.png`)}
							alt="Astronauts"
							style={{ maxWidth: 300, border: '10px solid #FFFFFF' }}
							id="Astronauts"
							onClick={toggleAstro}
						/>
					</Badge>
				</Grid>
				<Grid item xs={12} sm={6} md={4}>
					<Badge max={999} badgeContent={Object.keys(countriesSearchMatches).length} color="primary">
						<img
							src={require(`../../images/NASA_fonts/MaterialDark/countries_white_nooutline.png`)}
							alt="Countries"
							style={{ maxWidth: 300, border: '10px solid #FFFFFF' }}
							id="Countries"
							onClick={toggleCountry}
						/>
					</Badge>
				</Grid>
				<Grid item xs={12} sm={6} md={4}>
					<Badge max={999} badgeContent={Object.keys(satellitesSearchMatches).length} color="primary">
						<img
							src={require(`../../images/NASA_fonts/MaterialDark/satellites_white_nooutline.png`)}
							alt="Satellites"
							style={{ maxWidth: 300, border: '10px solid #FFFFFF' }}
							id="Satellites"
							onClick={toggleSatellite}
						/>
					</Badge>
				</Grid>
			</Grid>
			<br />
			<br />
			{renderSearchResults(astronautSearchMatches, pageAstro, showAstro, 'astronauts')}
			{renderSearchResults(countriesSearchMatches, pageCountry, showCountry, 'countries')}
			{renderSearchResults(satellitesSearchMatches, pageSatellite, showSatellite, 'satellites')}
		</div>
	);
}
