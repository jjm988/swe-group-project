import '../../css/no_match.css';
import React from 'react';
import DVD from '../../images/404_withSpaces.png';

export default function NoMatch() {
	return (
		<div>
			<div id="wrapper">
				<div id="ball">
					{' '}
					<img src={DVD} alt="404" />
				</div>
			</div>
		</div>
	);
}
