import React from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
	icon: {
		marginRight: theme.spacing(2)
	},
	cardGrid: {
		paddingTop: theme.spacing(8),
		paddingBottom: theme.spacing(8)
	},
	card: {
		height: '100%',
		display: 'flex',
		flexDirection: 'column'
	},
	cardMedia: {
		paddingTop: '56.25%' // 16:9
	},
	cardContent: {
		flexGrow: 1
	},
	footer: {
		backgroundColor: theme.palette.background.paper,
		padding: theme.spacing(6)
	}
}));

export default function GettingStarted() {
	const classes = useStyles();

	return (
		<React.Fragment>
			<br />
			<br />
			<br />
			<br />
			<main>
				<div className={classes.heroContent}>
					<Container maxWidth="sm">
						<Typography component="h1" variant="h2" align="center" color="textSecondary" gutterBottom>
							Land it in Space
						</Typography>
						<Typography variant="h5" align="center" color="textSecondary" paragraph>
							We are a group of computer science students at the University of Texas at Austin passionate
							about Space. For our project in the Spring 2020 Software Engineering course (CS 373), we
							were inspired to look towards the final frontier by the wonders of space and the rapid
							modern day advances made with companies such as SpaceX leading the new Space Race.
						</Typography>
					</Container>
				</div>
				<Container className={classes.cardGrid} maxWidth="md">
					<Grid container spacing={4}>
						<Grid item xs={12} sm={6} md={4}>
							<Card className={classes.card}>
								<CardMedia
									className={classes.cardMedia}
									image={require(`../../images/astronauts.jpg`)}
									title="Image title"
								/>
								<CardContent className={classes.cardContent}>
									<Typography gutterBottom variant="h5" component="h2" color="secondary">
										Astronauts
									</Typography>
									<Typography color="secondary">
										A collection of people from all backgrounds who have been to space!
									</Typography>
								</CardContent>
								<CardActions>
									<Button size="small" color="secondary" href="/astronauts">
										Learn More
									</Button>
								</CardActions>
							</Card>
						</Grid>

						<Grid item xs={12} sm={6} md={4}>
							<Card className={classes.card}>
								<CardMedia
									className={classes.cardMedia}
									image={require(`../../images/countries.jpg`)}
									title="Image title"
								/>
								<CardContent className={classes.cardContent}>
									<Typography gutterBottom variant="h5" component="h2" color="secondary">
										Countries
									</Typography>
									<Typography color="secondary">
										Countries that are involved in exploring the opportunities presented by space!
									</Typography>
								</CardContent>
								<CardActions>
									<Button size="small" color="secondary" href="/countries">
										Learn More
									</Button>
								</CardActions>
							</Card>
						</Grid>

						<Grid item xs={12} sm={6} md={4}>
							<Card className={classes.card}>
								<CardMedia
									className={classes.cardMedia}
									image={require(`../../images/satellites.jpg`)}
									title="Image title"
								/>
								<CardContent className={classes.cardContent}>
									<Typography gutterBottom variant="h5" component="h2" color="secondary">
										Satellites
									</Typography>
									<Typography color="secondary">
										Man-made objects that orbit our Earth collecting information and helping with
										communication!
									</Typography>
								</CardContent>
								<CardActions>
									<Button size="small" color="secondary" href="/satellites">
										Learn More
									</Button>
								</CardActions>
							</Card>
						</Grid>
					</Grid>
				</Container>
			</main>
			{/* Footer */}
			<footer className={classes.footer}>
				<Typography variant="subtitle1" align="center" color="textSecondary" component="p">
					Let's explore the final frontier!
				</Typography>
			</footer>
			{/* End footer */}
		</React.Fragment>
	);
}
