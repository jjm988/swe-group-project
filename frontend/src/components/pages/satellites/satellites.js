import React, { useEffect, useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import TableCell from '@material-ui/core/TableCell';
import axios from 'axios';
import Paper from '@material-ui/core/Paper';
import Link from '@material-ui/core/Link';
import background from '../../../images/8kStarField.png';
import Button from '@material-ui/core/Button';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import '../../../css/modelPage.css';
import ReactLoading from 'react-loading';

import {
	SortingState,
	IntegratedSorting,
	FilteringState,
	IntegratedFiltering,
	PagingState,
	IntegratedPaging
} from '@devexpress/dx-react-grid';

import {
	Grid,
	Table,
	TableHeaderRow,
	TableFilterRow,
	PagingPanel,
	Toolbar
} from '@devexpress/dx-react-grid-material-ui';

// adding styles
const styles = (theme) => ({
	cell: {
		width: '100%',
		padding: theme.spacing(1)
	},
	input: {
		color: 'white',
		fontSize: '14px',
		width: '100%'
	},
	pager: {
		'& button': {
			color: 'white'
		}
	}
});

// -------------------------PAGINATION-------------------------------------

const MyPager = ({ classes, ...restProps }) => {
	return <PagingPanel.Container {...restProps} className={`${classes.pager} custom-pager`} />;
};

export const MyPagerComponent = withStyles(styles, { name: 'MyPager' })(MyPager);

// -------------------------TABLE -------------------------------------

export default function EnhancedTable() {
	const [ satellites, setSatellites ] = React.useState([]);
	const [ search, setSearch ] = React.useState('');

	useEffect(() => {
		axios
			.get(`https://api.landitinspace.net/api/satellites`)
			.then((res) => {
				const astro = res.data;
				setSatellites((prevState) => prevState.concat(astro.objects));
			})
			.catch((err) => console.log(err));
	}, []);
	// -------------------------SEARCH-------------------------------------

	function getHighlightedText(text, highlight) {
		// Split on highlight term and include term into parts, ignore case
		const keywordsString = highlight.toLowerCase();

		const keywords = keywordsString.split(' ');

		// equivalent to: /(cake|pie|cookies)/g
		const pattern = new RegExp(`(${keywords.join('|')})`, 'gi');
		const parts = text.split(pattern);

		return (
			<span>
				{' '}
				{parts.map((part, i) => (
					<span key={i} style={keywords.includes(part.toLowerCase()) ? { backgroundColor: 'red' } : {}}>
						{part}
					</span>
				))}{' '}
			</span>
		);
	}

	// -------------------------CELL-------------------------------------

	const CellColor = ({ value, style, ...restProps }) => (
		<Table.Cell>
			<span style={{ color: 'white' }}>{value}</span>
		</Table.Cell>
	);

	const Cell = (props) => {
		return <CellColor {...props} />;
	};

	// -------------------------FILTER-------------------------------------

	const UnitsFilterCellBase = ({ filter, onFilter, classes }) => (
		<TableCell className="cell">
			<Input
				className="input"
				value={filter ? filter.value : ''}
				onChange={(e) => onFilter(e.target.value ? { value: e.target.value } : null)}
				placeholder="Filter..."
				inputProps={{
					style: { color: 'white', textAlign: 'left', height: 'inherit' },
					min: 1,
					max: 4
				}}
			/>
		</TableCell>
	);

	const FilterCell = (props) => {
		// hides filter option for image column
		if (props.column.name === 'img') {
			return <TableFilterRow.Cell {...props}>&nbsp;</TableFilterRow.Cell>;
		}
		return <UnitsFilterCellBase {...props} />;
	};

	// -------------------------LINK-------------------------------------

	const LinkFormatterBase = ({ row, value, href, actionClicked, ...restProps }) => (
		<TableCell>
			<span
				style={{
					cursor: 'pointer'
				}}
				onClick={(e) => actionClicked(row)}
			>
				<Link to={`/satellites/${row.satellite}`}>
					<a href={`/satellites/${row.satellite}`}>
						<p
							style={{
								color: 'white',
								margin: 0
							}}
						>
							{getHighlightedText(value.toString(), search)}
						</p>
					</a>
				</Link>
			</span>
		</TableCell>
	);

	const LinkTypeProvider = (props) => {
		const { column } = props;
		return <LinkFormatterBase {...props} />;
	};

	// --------------------------  IMG -------------------------------------

	const ImgFormatterBase = ({ row, value, href, actionClicked }) => (
		<Table.Cell>
			<span
				style={{
					cursor: 'pointer'
				}}
				onClick={(e) => actionClicked(row)}
			>
				<Link to={`/satellites/${row.satellite}`}>
					<a href={`/satellites/${row.satellite}`}>
						<p
							style={{
								color: 'white',
								margin: 0
							}}
						>
							<img
								src={row.img_link}
								style={{
									margin: '0 auto',
									borderRadius: '50%'
								}}
								className="image-cropper"
								alt="Avatar"
							/>
						</p>
					</a>
				</Link>
			</span>
		</Table.Cell>
	);

	const ImgProvider = (props) => {
		if (props.column.name === 'img') {
			return <ImgFormatterBase {...props} />;
		}
		return <LinkFormatterBase {...props} />;
	};

	// -------------------------SORT-------------------------------------

	const SortingIcon = ({ direction }) =>
		direction === 'asc' ? (
			<ArrowUpward style={{ fontSize: '18px' }} />
		) : (
			<ArrowDownward style={{ fontSize: '18px' }} />
		);

	const SortLabel = ({ onSort, children, direction }) => (
		<Button size="small" variant="contained" onClick={onSort} style={{ backgroundColor: 'DarkGrey' }}>
			{children}
			{direction && <SortingIcon direction={direction} />}
		</Button>
	);

	const NotSort = ({ onSort, children, direction }) => (
		<Button size="small" variant="contained" onClick={onSort} style={{ backgroundColor: 'DarkGrey' }}>
			{children}
		</Button>
	);

	const toSortLabel = (props) => {
		//doesn't show sort icons for image icon
		if (props.column.name === 'img') {
			return <NotSort {...props} />;
		}
		return <SortLabel {...props} />;
	};

	// -------------------------COLUMNS------------------------------------

	const [ columns ] = useState([
		{ name: 'img', title: 'Image' },
		{ name: 'satellite', title: 'Satellites' },
		{ name: 'date_of_launch', title: 'Date of Launch' },
		{ name: 'country', title: 'Country' },
		{ name: 'launch_vehicle', title: 'Launch Vehicle' },
		{ name: 'purpose', title: 'Purpose' }
	]);

	const [ sorting, setSorting ] = useState([ { columnName: 'Name', direction: 'asc' } ]);

	const [ sortingStateColumnExtensions ] = useState([ { columnName: 'img', sortingEnabled: false } ]);

	//Sets sorting guidelines for the date
	const comparePriority = (a, b) => {
		var priorityA = a.split('/');
		var priorityB = b.split('/');
		if (priorityA[2] < priorityB[2]) {
			return -1;
		} else if (priorityA[2] > priorityB[2]) {
			return 1;
		}
		if (priorityA[0] < priorityB[0]) {
			return -1;
		} else if (priorityA[0] > priorityB[0]) {
			return 1;
		}
		if (priorityA[1] === priorityB[1]) {
			return 0;
		}
		return priorityA[1] < priorityB[1] ? -1 : 1;
	};

	const [ integratedSortingColumnExtensions ] = useState([
		{ columnName: 'date_of_launch', compare: comparePriority }
	]);

	let searchResults = new Set();
	var res = search.split(' ');
	res.push(search);
	for (var satellite of satellites) {
		var flag = false;
		for (var searchTerm of res) {
			if (satellite.satellite.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
				flag = true;
			}
			if (satellite.date_of_launch.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
				flag = true;
			}
			if (satellite.country.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
				flag = true;
			}
			if (satellite.launch_vehicle.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
				flag = true;
			}
			if (satellite.purpose.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
				flag = true;
			}
		}
		if (flag === true) {
			searchResults.add(satellite);
		}
	}

	return (
		<div>
			{satellites.length != 0 ? (
				<div style={{ height: '100vh', backgroundImage: `url(${background})` }}>
					<br />
					<br />
					<br />
					<Paper style={{ backgroundImage: `url(${background})` }}>
						<br />
						<br />
						<Grid rows={Array.from(searchResults)} columns={columns}>
							<SortingState
								sorting={sorting}
								onSortingChange={setSorting}
								columnExtensions={sortingStateColumnExtensions}
							/>
							<IntegratedSorting columnExtensions={integratedSortingColumnExtensions} />

							<FilteringState />
							<IntegratedFiltering />

							<PagingState defaultCurrentPage={0} pageSize={10} />
							<IntegratedPaging />

							<Table cellComponent={(Cell, LinkTypeProvider, ImgProvider)} />

							<Toolbar />
							<img
								src={require(`../../../images/NASA_fonts/MaterialDark/satellites_white_nooutline.png`)}
								alt="Satellites"
								style={{ maxWidth: 500, boxShadow: '5px 5px 30px -10px blue' }}
							/>
							<br />
							<br />
							<br />
							<br />
							<br />
							<input
								style={{ fontSize: 25, paddingLeft: 10 }}
								className="SearchBar"
								type="text"
								onChange={(event) => setSearch(event.target.value)}
								placeholder="Search"
							/>

							<TableHeaderRow showSortingControls sortLabelComponent={toSortLabel} />
							<TableFilterRow cellComponent={FilterCell} />
							<PagingPanel containerComponent={MyPagerComponent} />
						</Grid>
					</Paper>
				</div>
			) : (
				<div className="load">
					<ReactLoading className="Sup" type={'bars'} color={'blue'} />
				</div>
			)}
		</div>
	);
}
