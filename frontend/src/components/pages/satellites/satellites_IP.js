import React, { useEffect } from 'react';
import axios from 'axios';
import '../../../css/modelPage.css';
import ReactLoading from 'react-loading';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import Avatar from '@material-ui/core/Avatar';
import background from '../../../images/8kStarField.png';
import NoMatch from '../no_match';
import { BrowserRouter, Route } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
	root: {
		margin: 'auto',
		minWidth: 280,
		maxWidth: 345,
		alignContent: 'center',
		padding: '20px',
		display: 'flex',
		flexDirection: 'column',
		fontSize: 18,
		backgroundImage: `url(${background})`
	},
	media: {
		height: 400
	},
	large: {
		width: theme.spacing(30),
		height: theme.spacing(30),
		margin: 'auto'
	},
	card: {
		display: 'flex',
		flexDirection: 'column'
	},
	avatar: {
		width: 60,
		height: 60,
		margin: 'auto'
	}
}));

export default function EnhancedTable(props) {
	const [ data, setData ] = React.useState([]);
	const [ done, setDone ] = React.useState(false);

	useEffect(() => {
		axios
			.get(`https://api.landitinspace.net/api/satellites`)
			.then((res) => {
				const data = res.data.objects;
				// console.log(data)
				var pages = window.location.pathname.split('/');
				var last = decodeURI(pages[pages.length - 1]);
				var i;
				var flag = false;
				for (i = 0; i < data.length; i++) {
					if (data[i].satellite === last + ' ') {
						setData(data[i]);
						flag = true;
						break;
					}
				}
				if (!flag) {
					setDone(true);
				}
			})
			.catch((err) => {
				console.log(err);
			});
	}, []);

	const classes = useStyles();
	var astro = ('' + data.astronauts).split(',');
	var wiki_summary = ('' + data.summary).split('wikipedia:');

	return (
		<div>
			{data.length == 0 ? (
				<div>
					{!done ? (
						<div className="load">
							<ReactLoading className="Sup" type={'bars'} color={'blue'} />
						</div>
					) : (
						<div>
							<BrowserRouter>
								<Route component={NoMatch} />
							</BrowserRouter>
						</div>
					)}
				</div>
			) : (
				<div style={{ backgroundImage: `url(${background})` }}>
					<br />
					<br />
					<br />
					<br />
					<br />
					<h1 className="underline1" style={{ color: 'orange' }}>
						{' '}
						{data.satellite}{' '}
					</h1>
					<br />
					<br />

					<Avatar
						style={useStyles.avatar}
						alt={data.satellite}
						src={data.img_link}
						className={classes.large}
					/>
					<br />
					<br />

					<p style={{ fontSize: 20, color: 'red' }}>Summary: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{wiki_summary[0] + 'wikipedia: '}
					</p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						<Link to={`${wiki_summary[1]}`}>
							<a href={`${wiki_summary[1]}`} rel="noopener noreferrer" target="_blank">
								{' '}
								{wiki_summary[1]}{' '}
							</a>
						</Link>
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Country: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						<Link to={`/countries/${data.country}`}>
							<a href={`/countries/${data.country}`}> {data.country} </a>
						</Link>
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Date of Launch: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.date_of_launch}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}> Astronauts: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						<Link to={`/astronauts/${astro[0]}`}>
							<a href={`/astronauts/${astro[0]}`}> {astro[0]} </a>
						</Link>,
						<Link to={`/astronauts/${astro[1]}`}>
							<a href={`/astronauts/${astro[1]}`}> {astro[1]} </a>
						</Link>
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Launch Site: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.launch_site}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Launch Vehicle: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.launch_vehicle}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Launch Mass(kg): </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.launch_mass_kg}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Operator: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.operator}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Class of Orbit: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.class_of_orbit}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Expected Lifetime(yr): </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.expected_lifetime_yr}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Purpose: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.purpose}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Users: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.users}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Period(minutes): </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.period_minute}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Apogee(km): </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.apogee_km}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Perigee(km): </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.perigee_km}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Longitude(degrees): </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						{data.longitude_degree}
					</p>

					<p style={{ fontSize: 20, color: 'red' }}>Source: </p>
					<p className="Setmargin" style={{ fontSize: 18 }}>
						<Link to={data.source}>
							<a href={data.source} rel="noopener noreferrer" target="_blank">
								{' '}
								{data.source}{' '}
							</a>
						</Link>
					</p>
					<br />
					<br />

					<div class="video-frame" style={{ overflow: 'hidden' }}>
						<iframe
							id="player2"
							width="700"
							title="Satellite Video"
							height="400"
							src={data.vid_link}
							frameborder="0"
							allow="autoplay; encrypted-media"
							allowfullscreen
						/>
					</div>
					<br />
					<br />
					<br />
				</div>
			)}
		</div>
	);
}
