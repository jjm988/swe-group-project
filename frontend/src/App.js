import React from 'react';
import './App.css';
import './css/splash.css';
import Splash_Slideshow from './components/parts/slideshow.js';
import Drawer from './components/parts/navigation.js';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

//------------------
//---Import Pages---
//------------------

import About from './components/pages/about';
import GettingStarted from './components/pages/gettingStarted';
import NoMatch from './components/pages/no_match';

//Models
import Astronauts from './components/pages/astronauts/astronauts';
import Countries from './components/pages/countries/countries';
import Satellites from './components/pages/satellites/satellites';

//Instances
import AstronautIP from './components/pages/astronauts/astronaut_IP.js';
import CountryIP from './components/pages/countries/country_IP';
import SatellitesIP from './components/pages/satellites/satellites_IP.js';
import Search from './components/pages/search.js';
import Visualization from './components/pages/visualization.js';

function App() {
	return (
		<div style={{ backgroundColor: '0x121212' }} className="App">
			<BrowserRouter>
				<Drawer />
				<Switch>
					<Route path="/about" component={About} />
					<Route path="/gettingStarted" component={GettingStarted} />
					<Route path="/astronauts/N/A" component={Astronauts} />
					<Route path="/astronauts/undefined" component={Astronauts} />
					<Route path="/astronauts/:id" component={AstronautIP} />
					<Route path="/astronauts" component={Astronauts} />
					<Route path="/satellites/N/A" component={Satellites} />
					<Route path="/satellites/undefined" component={Satellites} />
					<Route path="/satellites/:id" component={SatellitesIP} />
					<Route path="/satellites" component={Satellites} />
					<Route path="/countries/N/A" component={Countries} />
					<Route path="/countries/undefined" component={Countries} />
					<Route path="/countries/:id" component={CountryIP} />
					<Route path="/countries" component={Countries} />
					<Route path="/search" component={Search} />
					<Route path="/visualizations" component={Visualization} />
					<Route path="/" exact component={Splash_Slideshow} />
					<Route component={NoMatch} />
				</Switch>
			</BrowserRouter>
		</div>
	);
}
export default App;
