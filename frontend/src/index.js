import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core';

const theme = createMuiTheme({
	palette: {
		primary: {
			main: '#1a237e'
		},
		secondary: {
			dark: '#ffffff',
			main: '#ffffff'
		},
		background: {
			paper: '#303030',
			default: '#121212'
		},
		text: {
			secondary: '#ffffff'
		}
	}
});
ReactDOM.render(
	<MuiThemeProvider theme={theme}>
		<App />
	</MuiThemeProvider>,
	document.getElementById('root')
);

serviceWorker.unregister();
