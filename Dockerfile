FROM gpdowning/python

# The port our application will use
EXPOSE 8888

# Update linux image
RUN apt-get update

# Install Flask
RUN pip install Flask

RUN pip install Flask-Restless

RUN pip install Flask-SQLAlchemy

# Install Nodejs Dependancies
# RUN apt-get install git-core curl build-essential openssl libssl-dev

# Install Nodejs ppa
RUN curl -sL https://deb.nodesource.com/setup_13.x | bash -

# Install Nodejs
RUN apt-get install -y nodejs

# Dependancy for node to work
RUN apt-get install -y build-essential

RUN pip install flask-mysql

RUN pip install wikipedia

RUN apt-get install tmux -y

# RUN pip3 install selenium

CMD bash
