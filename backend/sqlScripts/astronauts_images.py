import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

from azure.cognitiveservices.search.imagesearch import ImageSearchClient
from msrest.authentication import CognitiveServicesCredentials

import requests
import matplotlib.pyplot as plt
from PIL import Image
from io import BytesIO



client = ImageSearchClient(endpoint=os.environ['FREE_ENDPOINT'], credentials=CognitiveServicesCredentials(os.environ['FREE_KEY']))
headers = {"Ocp-Apim-Subscription-Key" : os.environ['FREE_KEY']}

astronauts_imgs = {}


def generate_img_link(astronaut):
    params  = {"q": astronaut, "license": "public", "imageType": "photo"}
    response = requests.get(free_endpoint, headers=headers, params=params)
    response.raise_for_status()
    search_results = response.json()
    # print(search_results)
    thumbnail_url = [img["thumbnailUrl"] for img in search_results["value"][:1]]
    if len(thumbnail_url) == 0:
        url = 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Bruce_McCandless_II_during_EVA_in_1984.jpg/1200px-Bruce_McCandless_II_during_EVA_in_1984.jpg'
        return url
    return thumbnail_url[0]



def insert_img_links():
    try:
        connection = mysql.connector.connect(host='landitinspacedb.ckq85yquf2gl.us-east-1.rds.amazonaws.com',
                                             database='landitinspaceDB',
                                             user='backend',
                                             password='1234')

        query = 'SELECT DISTINCT(name) FROM astronauts'

        cursor = connection.cursor(buffered=True)
        cursor.execute(query)
        connection.commit()
        records = cursor.fetchall()
        it = iter(records)

        for row in it:
            astronaut = row[0]
            astronauts_imgs[astronaut] = generate_img_link('astronaut ' + astronaut)
            insert_query = "UPDATE astronauts SET img_link = '{}' WHERE name = '{}'".format(astronauts_imgs[astronaut], astronaut)
            cursor.execute(insert_query)


            print(astronaut, " ", astronauts_imgs[astronaut])
        connection.commit()
        cursor.close()


    except mysql.connector.Error as error:
        print("Failed to execute a query {}".format(error))


if __name__ == '__main__':
    insert_img_links()
    # main()
    # insert_img_links()
    # get_mail_codes()
