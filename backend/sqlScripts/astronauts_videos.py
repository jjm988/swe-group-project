import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

from youtube_search import YoutubeSearch
import os



def insert_vid_links():
    try:
        connection = mysql.connector.connect(host='landitinspacedb.ckq85yquf2gl.us-east-1.rds.amazonaws.com',
                                             database='landitinspaceDB',
                                             user='backend',
                                             password='1234')

        query = 'SELECT DISTINCT(name) FROM astronauts'

        cursor = connection.cursor(buffered=True)
        cursor.execute(query)
        connection.commit()
        records = cursor.fetchall()
        for row in records:
            # print(row[0])
            astronaut = row[0]
            search_query = "astronaut " + astronaut
            youtube_results = YoutubeSearch(search_query, max_results=1).to_dict()
            if len(youtube_results) == 0:
                link = "https://www.youtube.com/watch?v=_ojzEqbMZBE"
            else:
                youtube_obj = youtube_results[0]
                link = "https://www.youtube.com" + youtube_obj['link']

            # satellites_imgs[satellite] = generate_img_link(satellite)
            insert_query = "UPDATE astronauts SET vid_link = '{}' WHERE name = \"{}\"".format(link, astronaut)
            # print(insert_query)
            cursor.execute(insert_query)
            connection.commit()

            print(astronaut, " ", link)
        # print(astronauts_imgs)

        cursor.close()


    except mysql.connector.Error as error:
        print("Failed to execute a query {}".format(error))




if __name__ == '__main__':
    # insert_vid_links()
