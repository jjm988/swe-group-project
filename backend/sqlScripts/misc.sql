select * from countries;
select * from satellites;
select * from astronauts;

SELECT *
FROM astronauts
WHERE name LIKE '%l%';

select * from astrocopy;

select name from satellites
where name like "";


select distinct(satellite) from satellites;

UPDATE countries c
SET users =
(
select GROUP_CONCAT(Distinct(s.users)) as sc
FROM landitinspaceDB.satellites s
WHERE s.country = c.country
GROUP BY s.country
);




select distinct(country) from astronauts;

select count(name) from astronauts
where country = 'United States';


select a.country, a.name
from astronauts a, countries c
where a.country = c.country;

-- update count_satellites
UPDATE countries c
SET count_satellites =
(
select count(Distinct(s.satellite)) as sc
FROM satellites s
WHERE s.country = c.country
GROUP BY s.country
);

-- update count_astronauts
UPDATE countries c
SET count_astronauts =
(
select count(Distinct(s.name)) as sc
FROM astronauts s
WHERE s.country = c.country
GROUP BY s.country
);


-- number of satellites--
select count(Distinct(s.satellite)) as sc
FROM satellites s, countries c
WHERE s.country = c.country
GROUP BY s.country;

-- number of astronauts--
select s.country, count(Distinct(s.name)) as sc
FROM astronauts s, countries c
WHERE s.country = c.country
GROUP BY s.country;


UPDATE countries c
SET astronauts =
(
select GROUP_CONCAT(Distinct(a.name)) as an
FROM landitinspaceDB.astronauts a
WHERE a.country = c.country
GROUP BY a.country
);

select GROUP_CONCAT(Distinct(a.name)) as an
FROM landitinspaceDB.astronauts a
WHERE a.country = "United States";

-- adding united states data
UPDATE countries c
SET astronauts =
(
select GROUP_CONCAT(Distinct(a.name)) as an
FROM landitinspaceDB.astronauts a
WHERE a.country = "United STates"
GROUP BY a.country
)
WHERE country = "United States";

select LENGTH(astronauts) from countries where country like 'United States';


-- testing astro copy
CREATE TABLE astrocopy LIKE astronauts;
INSERT INTO astrocopy SELECT * FROM astronauts;

select * from astrocopy;

-- satellites
UPDATE astronauts a
SET satellites =
(
select c.satellites
FROM countries c
WHERE a.country = c.country
);

-- purposes
UPDATE astronauts a
SET purposes =
(
select c.purposes
FROM countries c
WHERE a.country = c.country
);

-- users
UPDATE astronauts a
SET users =
(
select c.users
FROM countries c
WHERE a.country = c.country
);

-- operators
UPDATE astronauts a
SET operators =
(
select c.operators
FROM countries c
WHERE a.country = c.country
);

CREATE TABLE countriescopy LIKE countries;
INSERT INTO countriescopy SELECT * FROM countries;
select * from countriescopy;


SELECT * FROM satellites;

-- working on countries data
SELECT * FROM countriescopy;
SELECT * FROM countries;

UPDATE countries
SET astronauts = 'N/A'
WHERE astronauts is NULL;

UPDATE countries
SET count_astronauts = 0
WHERE count_astronauts is NULL;

UPDATE countries
SET users = 'N/A'
WHERE users is NULL;

-- working on astronauts data
SELECT * FROM astronauts;

UPDATE astronauts
SET satellites = 'N/A'
WHERE satellites is NULL;

UPDATE astronauts
SET purposes = 'N/A'
WHERE purposes is NULL;

UPDATE astronauts
SET users = 'N/A'
WHERE users is NULL;

UPDATE astronauts
SET operators = 'N/A'
WHERE operators is NULL;

-- working on satellites data
SELECT * FROM satellites;

SELECT * FROM astronauts;


UPDATE satellites
SET expected_lifetime_yr = 'N/A'
WHERE expected_lifetime_yr = '';

UPDATE satellites
SET astronauts = 'N/A'
WHERE astronauts is NULL;

UPDATE satellites c
SET astronauts =
(
select GROUP_CONCAT(Distinct(a.name)) as an
FROM astronauts a
WHERE a.country = c.country
GROUP BY a.country
);

select GROUP_CONCAT(Distinct(a.name)) as an
FROM astronauts a
WHERE a.country = 'United States';

UPDATE satellites c
SET astronauts = 'Alan Bean,Alan G. Poindexter,Alan Shepard,Albert Sacco,Alfred Worden,Andrew J. Feustel,Andrew M. Allen,Andrew R. Morgan,Andrew Thomas,Anna Lee Fisher, M.D.,Anne McClain,Anthony W. England,B. Alvin Drew,Barbara Morgan,Barry Wilmore,Bernard A. Harris, Jr.,Bonnie J. Dunbar,Brent W. Jett, Jr.,Brewster Shaw,Brian Binnie,Brian Duffy,Bruce McCandless II,Bruce Melnick,Bryan OConnor,Buzz Aldrin,Byron Lichtenberg,Carl Meade,Carl Walz,Catherine Coleman,Charles Bolden,Charles Camarda,Charles D. Walker,Charles Duke,Charles E. Brady, Jr., M.D.,Charles Fullerton,Charles Gemar,Charles Hobaugh,Charles Pete Conrad,Charles Precourt,Charles Veach,Christina Koch,Christopher Cassidy,Christopher Ferguson,Clayton Anderson,Curtis Brown,Dale Gardner,Daniel Barry,Daniel Brandenstein,Daniel Burbank,Daniel Bursch,Daniel Tani,David Griggs,David Hilmers,David Leestma,David M. Brown,David M. Walker,David Scott,David Wolf, M.D.,Dennis Tito,Dick Scobee,Dominic A. Antonelli,Dominic Gorie,Don Lind,Donald Deke Slayton,Donald McMonagle,Donald Pet'
WHERE country = 'USA'
