import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

from youtube_search import YoutubeSearch
import os



def insert_vid_links():
    try:
        connection = mysql.connector.connect(host='landitinspacedb.ckq85yquf2gl.us-east-1.rds.amazonaws.com',
                                             database='landitinspaceDB',
                                             user='backend',
                                             password='1234')

        query = 'SELECT DISTINCT(country) FROM countries'

        cursor = connection.cursor(buffered=True)
        cursor.execute(query)
        connection.commit()
        records = cursor.fetchall()
        for row in records:
            # print(row[0])
            country = row[0]
            search_query = "country " + country
            youtube_results = YoutubeSearch(search_query, max_results=1).to_dict()
            if len(youtube_results) == 0:
                link = "https://www.youtube.com/watch?v=rGcRe4lt4z4"
            else:
                youtube_obj = youtube_results[0]
                link = "https://www.youtube.com" + youtube_obj['link']

            insert_query = "UPDATE countries SET vid_link = '{}' WHERE country = \"{}\"".format(link, country)
            cursor.execute(insert_query)
            connection.commit()

            print(country, " ", link)

        cursor.close()


    except mysql.connector.Error as error:
        print("Failed to execute a query {}".format(error))




if __name__ == '__main__':
    # insert_vid_links()
