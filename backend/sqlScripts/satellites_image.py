import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

from azure.cognitiveservices.search.imagesearch import ImageSearchClient
from msrest.authentication import CognitiveServicesCredentials

import requests
import matplotlib.pyplot as plt
from PIL import Image
from io import BytesIO
import random

import os


# 922bb83b118a43198f8a38e25f863cc5
# 'https://api.cognitive.microsoft.com/bing/v7.0/images/search'
headers = {"Ocp-Apim-Subscription-Key" : os.environ['FREE_KEY']}

satellites_imgs = {}

satellite_imgs = ['https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTUcvMl2-0pRsp_W-3p7-EgqxVGGEt9pSp5AmktpXpxGKkiCLUM&usqp=CAU',\
'http://1.bp.blogspot.com/-3n3FV3yHmGE/UenxsHtms-I/AAAAAAAAIZo/sKuQGxTWY4U/s1600/TDRS+satellite.jpg', \
'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT2b9L5hMbEb_8PHM4MJcJu4Y-aOhmuP5ZWbDcBH0LxmvZs68Au&usqp=CAU', \
'https://www.nasa.gov/sites/default/files/styles/side_image/public/thumbnails/image/edu_jason-2_satellites.jpg?itok=oS5wpaVL', \
'https://cdn.talkingpointsmemo.com/wp-content/uploads/2014/03/rdonbkylgqndlgedz3xx.jpg', \
'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSFnfCQYdBjCA6exbmJM68vjVflp5oeRWP0XsZJ3Df_gN2C52a5&usqp=CAU', \
'https://cdn.vox-cdn.com/thumbor/YAtmFT74XjwjeHgyXVuY76SqjYE=/0x0:999x666/1200x800/filters:focal(0x0:999x666)/cdn.vox-cdn.com/assets/3663113/CubeSat-lead.png', \
'https://www.softwaretestingnews.co.uk/wp-content/uploads/2017/04/Satellite.png', \
'https://1l0044267psh26mr7fa57m09-wpengine.netdna-ssl.com/wp-content/uploads/2019/08/AA_Sept19_Satellites-1200x675.jpg', \
'https://cnet4.cbsistatic.com/img/-JKG69A9xmdlvxVwYtpIztVHxHI=/940x0/2018/08/21/09803db6-578f-41f7-9c7a-0b9efc5d6751/starshot-satellite-launch.jpg']


def generate_img_link(satellite):
    params  = {"q": satellite, "license": "public", "imageType": "photo"}
    response = requests.get(os.environ['FREE_ENDPOINT'], headers=headers, params=params)
    response.raise_for_status()
    search_results = response.json()
    # print(search_results)
    thumbnail_url = [img["thumbnailUrl"] for img in search_results["value"][:1]]
    if len(thumbnail_url) == 0:
        url = random.choice(satellite_imgs)
        return url
    return thumbnail_url[0]





def insert_img_links():
    try:
        connection = mysql.connector.connect(host='landitinspacedb.ckq85yquf2gl.us-east-1.rds.amazonaws.com',
                                             database='landitinspaceDB',
                                             user='backend',
                                             password='1234')

        query = 'SELECT DISTINCT(satellite) FROM satellites'

        cursor = connection.cursor(buffered=True)
        cursor.execute(query)
        connection.commit()
        records = cursor.fetchall()
        sate_itr = iter(records)
        for sate in sate_itr:
            if sate[0] == 'SAOCOM-1A ':
                break
        for sate in sate_itr:
            s = sate[0]
            url = random.choice(satellite_imgs)
            insert_query = "UPDATE satellites SET img_link = '{}' WHERE satellite = \"{}\"".format(url, s)
            cursor.execute(insert_query)
            print(s)

        # for row in records:
        #     # print(row[0])
        #     satellite = row[0]
        #
        #     satellites_imgs[satellite] = generate_img_link('satellite ' + satellite)
        #     insert_query = "UPDATE satellites SET img_link = '{}' WHERE satellite = \"{}\"".format(satellites_imgs[satellite], satellite)
        #     # print(insert_query)
        #     cursor.execute(insert_query)
        #     # connection.commit()
        #
        #     print(satellite)
        #     print(satellites_imgs[satellite])
        # print(astronauts_imgs)
        connection.commit()
        cursor.close()


    except mysql.connector.Error as error:
        print("Failed to execute a query {}".format(error))

def grab_link_from_txt():
    try:
        connection = mysql.connector.connect(host='landitinspacedb.ckq85yquf2gl.us-east-1.rds.amazonaws.com',
                                             database='landitinspaceDB',
                                             user='backend',
                                             password='1234')
        cursor = connection.cursor(buffered=True)

        file = open('sate_img_cache.txt', 'r')
        while True:
            s = file.readline().strip()
            if not s:
                break
            img = file.readline().strip()
            insert_query = "UPDATE satellites SET img_link = '{}' WHERE satellite LIKE \"{}\"".format(img, s+"%")
            cursor.execute(insert_query)

            print(s)
            print(img)
        connection.commit()
        cursor.close()

    except mysql.connector.Error as error:
        print("Failed to execute a query {}".format(error))

if __name__ == '__main__':
    # grab_link_from_txt()
    # print(os.environ['FREE_KEY'])
    # generate_img_link()
    insert_img_links()
    # main()
    # insert_img_links()
    # get_mail_codes()
