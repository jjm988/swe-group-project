import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

import json



def read_json():
    f = open('../WikiScraper/Summaries/satelliteSumV2.json')

    # returns JSON object as
    # a dictionary
    data = json.load(f)

    return data



def insert_paragraph():
    try:
        connection = mysql.connector.connect(host='landitinspacedb.ckq85yquf2gl.us-east-1.rds.amazonaws.com',
                                             database='landitinspaceDB',
                                             user='backend',
                                             password='1234')



        dictionary = read_json() # astro name : description
        # print(dictionary)
        cursor = connection.cursor(buffered=True)

        for s in dictionary:
            print(s)
            summary = dictionary[s]
            # print(paragraph)
            summary = summary.replace('"', '\'')
            summary = summary.replace("'", "''")

            s = s.replace("'", "''")
            query = "UPDATE satellites SET summary = '{}' WHERE satellite = '{}'".format(summary, s)
            cursor.execute(query)


        connection.commit()
        cursor.close()


    except mysql.connector.Error as error:
        print("Failed to execute a query {}".format(error))


if __name__ == '__main__':
    insert_paragraph()
    # test()
