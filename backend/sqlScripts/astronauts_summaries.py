import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

import json



def read_json():
    f = open('../WikiScraper/Summaries/astroSumV2.json')

    # returns JSON object as
    # a dictionary
    data = json.load(f)

    return data



def insert_paragraph():
    try:
        connection = mysql.connector.connect(host='landitinspacedb.ckq85yquf2gl.us-east-1.rds.amazonaws.com',
                                             database='landitinspaceDB',
                                             user='backend',
                                             password='1234')



        dictionary = read_json() # astro name : description
        cursor = connection.cursor(buffered=True)

        for i in dictionary:
            print(i)
            paragraph = dictionary[i]
            # print(paragraph)
            paragraph = paragraph.replace("'", "")
            paragraph = paragraph.replace("\"", "")

            # need to remove ' and "
            # print(paragraph)


            query = "UPDATE astronauts SET summary = '{}' WHERE name = '{}'".format(paragraph, i)
            print()
            # print(query)
            cursor.execute(query)


        connection.commit()
        cursor.close()


    except mysql.connector.Error as error:
        print("Failed to execute a query {}".format(error))

def test():
    dictionary = read_json() # astro name : description

    for i in dictionary:
        paragraph = dictionary[i]
        # print(paragraph)
        # paragraph = paragraph.replace("'", "")
        # paragraph = paragraph.replace("\"", "")
        print(i, ': ', paragraph)

if __name__ == '__main__':
    insert_paragraph()
    # test()
