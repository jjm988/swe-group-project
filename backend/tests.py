import unittest
from main import app

class TestStringMethods(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

#----------------------------TEST STATUS CODES-------------------------------

    def test_1(self):
        response = self.app.get('/api/astronauts')
        self.assertEqual(response.status_code, 200)

    def test_2(self):
        response = self.app.get('/api/satellites')
        self.assertEqual(response.status_code, 200)

    def test_3(self):
        response = self.app.get('/api/countries')
        self.assertEqual(response.status_code, 200)

#----------------------------TEST NUM INSTANCES-------------------------------

    def test_4(self):
        response = self.app.get('/api/astronauts')
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        self.assertEqual(len(response_json["objects"]), 566)
        self.assertEqual(response_json["page"], 1)
        self.assertEqual(response_json["total_pages"], 1)
        self.assertEqual(response_json["num_results"], 566)

    def test_5(self):
        response = self.app.get('/api/countries')
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        self.assertEqual(len(response_json["objects"]), 99)
        self.assertEqual(response_json["page"], 1)
        self.assertEqual(response_json["total_pages"], 1)
        self.assertEqual(response_json["num_results"], 99)

    def test_6(self):
        response = self.app.get('/api/satellites')
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        self.assertEqual(len(response_json["objects"]), 2166)
        self.assertEqual(response_json["page"], 1)
        self.assertEqual(response_json["total_pages"], 1)
        self.assertEqual(response_json["num_results"], 2166)

#----------------------------TEST FIELDS-------------------------------

    def test_7(self):
        response = self.app.get('/api/astronauts')
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        d = list(response_json["objects"][0].keys())
        assert len(d) == 13
        assert d[0] == 'country'
        assert d[1] == 'flights'
        assert d[2] == 'gender'
        assert d[3] == 'img_link'
        assert d[4] == 'name'
        assert d[5] == 'operators'
        assert d[6] == 'purposes'
        assert d[7] == 'satellites'
        assert d[8] == 'summary'
        assert d[9] == 'total_flight_time'
        assert d[10] == 'total_flights'
        assert d[11] == 'users'
        assert d[12] == 'vid_link'

    def test_8(self):
        response = self.app.get('/api/countries')
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        d = list(response_json["objects"][0].keys())
        assert len(d) == 13
        assert d[0] == 'astronauts'
        assert d[1] == 'count_astronauts'
        assert d[2] == 'count_satellites'
        assert d[3] == 'country'
        assert d[4] == 'img_link'
        assert d[5] == 'launch_sites'
        assert d[6] == 'launch_vehicles'
        assert d[7] == 'operators'
        assert d[8] == 'purposes'
        assert d[9] == 'satellites'
        assert d[10] == 'summary'
        assert d[11] == 'users'
        assert d[12] == 'vid_link'

    def test_9(self):
        response = self.app.get('/api/satellites')
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        d = list(response_json["objects"][0].keys())
        assert len(d) == 21
        assert d[0] == 'apogee_km'
        assert d[1] == 'astronauts'
        assert d[2] == 'class_of_orbit'
        assert d[3] == 'country'
        assert d[4] == 'date_of_launch'
        assert d[5] == 'expected_lifetime_yr'
        assert d[6] == 'img_link'
        assert d[7] == 'launch_mass_kg'
        assert d[8] == 'launch_site'
        assert d[9] == 'launch_vehicle'
        assert d[10] == 'longitude_degree'
        assert d[11] == 'operator'
        assert d[12] == 'perigee_km'
        assert d[13] == 'period_minute'
        assert d[14] == 'purpose'
        assert d[15] == 'sat_id'
        assert d[16] == 'satellite'
        assert d[17] == 'source'
        assert d[18] == 'summary'
        assert d[19] == 'users'
        assert d[20] == 'vid_link'

#--------------------------TEST INSTANCE ALL INFO-------------------------------

    def test_10(self):
        response = self.app.get('/api/astronauts/Neil%20Armstrong')
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        correct = {"country":"United States",
                    "flights":"Gemini 8 (1966), Apollo 11 (1969)",
                    "gender":"Man",
                    "img_link":"https://tse2.mm.bing.net/th?id=OIP.hqj-RI75ea8SiUk1oSGbmgAAAA&pid=Api",
                    "name":"Neil Armstrong",
                    "operators":"National Reconnaissance Office (NRO),US Air Force,SES S.A.,SES S.A./Gogo,University of California-Berkeley/Imperi,California Polytechnic State University,DoD/NOAA,Global Eagle Entertainment,\"Echostar Satellite Services, LLC/Intels,\"Echostar Satellite Services, LLC\",University of Texas - Austin,\"Firebird Consortium (Montana State Univ,Intelsat S.A. ,Globalstar,NOAA (National Oceanographic and Atmosph,Air Force Satellite Control Network,\"Space Sciences Laboratory, UC Berkeley/,Intelsat S.A./Sky Perfect JSAT Corp.,PanAmSat (Intelsat S.A.),NASA/CalPoly,National Reconnaissance Office (NRO)/US ,DoD/US Air Force,National Oceanographic and Atmospheric A,National Aeronautics and Space Administr,USAF /ORSO (Operationally Responsive Spa,Unknown US intelligence agency,Helios Wire,US Naval Academy,US Coast Guard,\"Tyvak Nanosatellite Systems, Inc.\",Strategic Space Command/Space Surveillan,\"Planet Labs, Inc.\",SpaceX,Missile Defense Agency (MDA),US Navy,Military Satellite Communications - US A,Department of Homeland Security",
                    "purposes":"Earth Observation,Communications,Space Science,Technology Development,Space Observation,Navigation/Global Positioning,Technology Demonstration,Communications/Technology Deve,Earth Observation/Technology D,Educational,Surveillance,Earth Science",
                    "satellites":"Advanced Orion 2, Advanced Orion 3, Advanced Orion 4, Advanced Orion 5, Advanced Orion 6, Advanced Orion 7, AEHF-2, AEHF-3, AEHF-4, AEHF-5, AMC-1, AMC-10, AMC-11, AMC-2, AMC-4, AMC-6, AMC-7, AMC-8, CBAS-1, CINEMA-1, DAVE, DMSP 5D-2 F14, DMSP 5D-3 F15, DMSP 5D-3 F16, DMSP 5D-3 F17, DMSP 5D-3 F18, DSCS III-A3, DSCS III-B6, DSCS III-F10, DSCS III-F11, DSCS III-F12, DSCS III-F9, EAGLE, Eagle-1, Echostar 9/Galaxy 23, Echostar G1, FAST 1, FIA Radar 1, FIA Radar 2, FIA Radar 3, FIA Radar 4, FIA Radar 5, Firebird-C, Firebird-D, Galaxy-25, Galaxy-28, Globalstar M073, Globalstar M074, Globalstar M075, Globalstar M076, Globalstar M077, Globalstar M078, Globalstar M079, Globalstar M080, Globalstar M081, Globalstar M082, Globalstar M083, Globalstar M084, Globalstar M085, Globalstar M086, Globalstar M088, Globalstar M089, Globalstar M090, Globalstar M091, Globalstar M092, Globalstar M093, Globalstar M094, Globalstar M095, Globalstar M096, Globalstar M097, GOES 13, GOES 14, GOES 15, GSSAP 1, GSSAP 2, GSSAP 3, GSSAP 4, HESSI",
                    "total_flight_time":"008:14:00",
                    "total_flights":2,
                    "users":"N/A",
                    "vid_link":"https://www.youtube.com/embed/qsNo8rcyaAs"
                    }
        for k in correct:
            assert correct[k] == response_json[k]


    def test_11(self):
        response = self.app.get('/api/astronauts/Janet%20L.%20Kavandi')
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        correct = {"country":"United States",
                    "flights":"STS-91 (1998), STS-99 (2000), STS-104 (2001)",
                    "gender":"Woman",
                    "img_link":"https://tse4.mm.bing.net/th?id=OIP.-x9lqkXDmFBV3YAquu9erAHaFJ&pid=Api",
                    "name":"Janet L. Kavandi",
                    "operators":"National Reconnaissance Office (NRO),US Air Force,SES S.A.,SES S.A./Gogo,University of California-Berkeley/Imperi,California Polytechnic State University,DoD/NOAA,Global Eagle Entertainment,\"Echostar Satellite Services, LLC/Intels,\"Echostar Satellite Services, LLC\",University of Texas - Austin,\"Firebird Consortium (Montana State Univ,Intelsat S.A. ,Globalstar,NOAA (National Oceanographic and Atmosph,Air Force Satellite Control Network,\"Space Sciences Laboratory, UC Berkeley/,Intelsat S.A./Sky Perfect JSAT Corp.,PanAmSat (Intelsat S.A.),NASA/CalPoly,National Reconnaissance Office (NRO)/US ,DoD/US Air Force,National Oceanographic and Atmospheric A,National Aeronautics and Space Administr,USAF /ORSO (Operationally Responsive Spa,Unknown US intelligence agency,Helios Wire,US Naval Academy,US Coast Guard,\"Tyvak Nanosatellite Systems, Inc.\",Strategic Space Command/Space Surveillan,\"Planet Labs, Inc.\",SpaceX,Missile Defense Agency (MDA),US Navy,Military Satellite Communications - US A,Department of Homeland Security",
                    "purposes":"Earth Observation,Communications,Space Science,Technology Development,Space Observation,Navigation/Global Positioning,Technology Demonstration,Communications/Technology Deve,Earth Observation/Technology D,Educational,Surveillance,Earth Science",
                    "satellites":"Advanced Orion 2, Advanced Orion 3, Advanced Orion 4, Advanced Orion 5, Advanced Orion 6, Advanced Orion 7, AEHF-2, AEHF-3, AEHF-4, AEHF-5, AMC-1, AMC-10, AMC-11, AMC-2, AMC-4, AMC-6, AMC-7, AMC-8, CBAS-1, CINEMA-1, DAVE, DMSP 5D-2 F14, DMSP 5D-3 F15, DMSP 5D-3 F16, DMSP 5D-3 F17, DMSP 5D-3 F18, DSCS III-A3, DSCS III-B6, DSCS III-F10, DSCS III-F11, DSCS III-F12, DSCS III-F9, EAGLE, Eagle-1, Echostar 9/Galaxy 23, Echostar G1, FAST 1, FIA Radar 1, FIA Radar 2, FIA Radar 3, FIA Radar 4, FIA Radar 5, Firebird-C, Firebird-D, Galaxy-25, Galaxy-28, Globalstar M073, Globalstar M074, Globalstar M075, Globalstar M076, Globalstar M077, Globalstar M078, Globalstar M079, Globalstar M080, Globalstar M081, Globalstar M082, Globalstar M083, Globalstar M084, Globalstar M085, Globalstar M086, Globalstar M088, Globalstar M089, Globalstar M090, Globalstar M091, Globalstar M092, Globalstar M093, Globalstar M094, Globalstar M095, Globalstar M096, Globalstar M097, GOES 13, GOES 14, GOES 15, GSSAP 1, GSSAP 2, GSSAP 3, GSSAP 4, HESSI",
                    "total_flight_time":"033:20:07",
                    "total_flights":3,
                    "users":"N/A",
                    "vid_link":"https://www.youtube.com/embed/czBwx2sbNqo"
                    }
        for k in correct:
            assert correct[k] == response_json[k]


    def test_12(self):
        response = self.app.get('/api/countries/United%20States')
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        correct = {"astronauts":"Alan Bean,Alan G. Poindexter,Alan Shepard,Albert Sacco,Alfred Worden,Andrew J. Feustel,Andrew M. Allen,Andrew R. Morgan,Andrew Thomas,Anna Lee Fisher, M.D.,Anne McClain,Anthony W. England,B. Alvin Drew,Barbara Morgan,Barry Wilmore,Bernard A. Harris, Jr.,Bonnie J. Dunbar,Brent W. Jett, Jr.,Brewster Shaw,Brian Binnie,Brian Duffy,Bruce McCandless II,Bruce Melnick,Bryan O'Connor,Buzz Aldrin,Byron Lichtenberg,Carl Meade,Carl Walz,Catherine Coleman,Charles \"Pete\"\" Conrad\",Charles Bolden,Charles Camarda,Charles D. Walker,Charles Duke,Charles E. Brady, Jr., M.D.,Charles Fullerton,Charles Gemar,Charles Hobaugh,Charles Precourt,Charles Veach,Christina Koch,Christopher Cassidy,Christopher Ferguson,Clayton Anderson,Curtis Brown,Dale Gardner,Daniel Barry,Daniel Brandenstein,Daniel Burbank,Daniel Bursch,Daniel Tani,David Griggs,David Hilmers,David Leestma,David M. Brown,David M. Walker,David Scott,David Wolf, M.D.,Dennis Tito,Dick Scobee,Dominic A. Antonelli,Dominic Gorie,Don Lind,Donald \"Deke\"\" Slayton\",Donald McMonagle,D",
                    "count_astronauts":346,
                    "count_satellites":948,
                    "country":"United States",
                    "img_link":"http://flags.ox3.in/svg/us.svg",
                    "launch_sites":"Cape Canaveral,Guiana Space Center,Baikonur Cosmodrome,Vandenberg AFB,Sea Launch Odyssey,Kodiak Launch Complex,Rocket Lab Launch Complex 1,International Space Station,Satish Dhawan Space Centre,Cygnus,Dombarovsky Air Base,Orbital ATK L-1011,Vostochny Cosmodrome,Kwajalein Island,Taiyuan Launch Center,Plesetsk Cosmodrome,Wallops Island Flight Facility,International Space Station - Cygnus,Dragon CRS-17",
                    "launch_vehicles":"Titan IVA,Titan IV,Delta 4 Heavy,Atlas 5,Atlas 2A,Atlas 2AS,Ariane 44L,Ariane 44LP,Proton K,Ariane 5G,Atlas  5,Delta 2,Titan 2,Delta 4,Zenit 3SL,Minotaur 4,Soyuz-Fregat,Soyuz.2.1a/Fregat,Soyuz 2.1a/Fregat,Soyuz 2.1a,Delta 4M,Delta 4M+,Pegasus,Zenit 3SLB,Ariane 5,Ariane 5 ECA,Electron,Titan 4B,Titan 4A,Falcon 9,Athena 1,Nanorack Deployer,Minotaur-C,Soyuz-2.1b,PSLV XL,Vega,Space Shuttle (STS 26),Space Shuttle (STS 43),Space Shuttle (STS 54),Space Shuttle (STS 70),Atlas 2,Atlas 3,Dnepr,Pegasus XL,Proton M,Delta 2310,PSLV,Falcon Heavy,Space Shuttle (STS 93),Breeze M,Proton M/Breeze M,Soyuz,Titan IVB,Proton,Atlas 3B,Soyuz 2.1b,Atlas Centaur,Soyuz U,Atlas,Zenit,Ariane 42L,Delta 2 7920,Long March 2C,Rokot/BrizKM,L1011,PSLV C-30,Antares 230,PSLV-XL,Delta 7920,Taurus,Kaber Microsatellite Deployer,Minotaur 1",
                    "operators":"National Reconnaissance Office (NRO),US Air Force,SES S.A.,SES S.A./Gogo,University of California-Berkeley/Imperi,California Polytechnic State University,DoD/NOAA,Global Eagle Entertainment,\"Echostar Satellite Services, LLC/Intels,\"Echostar Satellite Services, LLC\",University of Texas - Austin,\"Firebird Consortium (Montana State Univ,Intelsat S.A. ,Globalstar,NOAA (National Oceanographic and Atmosph,Air Force Satellite Control Network,\"Space Sciences Laboratory, UC Berkeley/,Intelsat S.A./Sky Perfect JSAT Corp.,PanAmSat (Intelsat S.A.),NASA/CalPoly,National Reconnaissance Office (NRO)/US ,DoD/US Air Force,National Oceanographic and Atmospheric A,National Aeronautics and Space Administr,USAF /ORSO (Operationally Responsive Spa,Unknown US intelligence agency,Helios Wire,US Naval Academy,US Coast Guard,\"Tyvak Nanosatellite Systems, Inc.\",Strategic Space Command/Space Surveillan,\"Planet Labs, Inc.\",SpaceX,Missile Defense Agency (MDA),US Navy,Military Satellite Communications - US A,Department of Homeland Security",
                    "purposes":"Earth Observation,Communications,Space Science,Technology Development,Space Observation,Navigation/Global Positioning,Technology Demonstration,Communications/Technology Deve,Earth Observation/Technology D,Educational,Surveillance,Earth Science",
                    "satellites":"Advanced Orion 2 ,Advanced Orion 3 ,Advanced Orion 4 ,Advanced Orion 5 ,Advanced Orion 6 ,Advanced Orion 7 ,AEHF-2 ,AEHF-3 ,AEHF-4 ,AEHF-5 ,AMC-1 ,AMC-10 ,AMC-11 ,AMC-2 ,AMC-4 ,AMC-6 ,AMC-7 ,AMC-8 ,CBAS-1 ,CINEMA-1 ,DAVE ,DMSP 5D-2 F14 ,DMSP 5D-3 F15 ,DMSP 5D-3 F16 ,DMSP 5D-3 F17 ,DMSP 5D-3 F18 ,DSCS III-A3 ,DSCS III-B6 ,DSCS III-F10 ,DSCS III-F11 ,DSCS III-F12 ,DSCS III-F9 ,EAGLE ,Eagle-1 ,Echostar 9/Galaxy 23 ,Echostar G1 ,FAST 1 ,FIA Radar 1 ,FIA Radar 2 ,FIA Radar 3 ,FIA Radar 4 ,FIA Radar 5 ,Firebird-C ,Firebird-D ,Galaxy-25 ,Galaxy-28 ,Globalstar M073 ,Globalstar M074 ,Globalstar M075 ,Globalstar M076 ,Globalstar M077 ,Globalstar M078 ,Globalstar M079 ,Globalstar M080 ,Globalstar M081 ,Globalstar M082 ,Globalstar M083 ,Globalstar M084 ,Globalstar M085 ,Globalstar M086 ,Globalstar M088 ,Globalstar M089 ,Globalstar M090 ,Globalstar M091 ,Globalstar M092 ,Globalstar M093 ,Globalstar M094 ,Globalstar M095 ,Globalstar M096 ,Globalstar M097 ,GOES 13 ,GOES 14 ,GOES 15 ,GSSAP 1 ,GSSAP 2 ,GSSAP 3 ,GSSAP 4 ,HESSI",
                    "summary":"\nThe United States of America (USA), commonly known as the United States (U.S. or US) or America, is a country consisting of 50 states, a federal district, five major self-governing territories, and various possessions. At 3.8 million square miles (9.8 million km2), it is the worlds third- or fourth... \nRead more at wikipedia: https://en.wikipedia.org/wiki/United_States",
                    "users":"N/A",
                    "vid_link":"https://www.youtube.com/embed/Gt2mYPwXyAc"}
        for k in correct:
            assert correct[k] == response_json[k]


    def test_13(self):
        response = self.app.get('/api/countries/India')
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        correct = {"astronauts":"Rakesh Sharma",
                    "count_astronauts":1,
                    "count_satellites":57,
                    "country":"India",
                    "img_link":"http://flags.ox3.in/svg/in.svg",
                    "launch_sites":"Satish Dhawan Space Centre,Guiana Space Center,Vandenberg AFB",
                    "launch_vehicles":"PSLV C16,PSLV,PSLV-XL,PSLV-C27,GSLV F04,Ariane 5 ECA,Ariane 5G,GSLV Mk.2,PSLV C5,PSLV XL,GSLV,PSLV C12,Ariane 5,PSLV C9,PSLV C6,PSLV C-30,Falcon 9,PSLV C17,GSLV MK.3,PSLV C7",
                    "operators":"Indian Space Research Organization (ISRO,\"College of Engineering, Pune\",SRM University,South Asian Association for Regional Coo,Ministry of Defense,Noorul Islam University,Defence Research and Development Organiz,Telemetry Tracking and Command Network (,Exseed Space,Indian Air Force,Indian Navy",
                    "purposes":"Space Science,Navigation/Regional Positionin,Communications,Earth Observation,Technology Development,Communications/Navigation",
                    "satellites":"Youthsat,IRNSS-1G ,IRNSS-1F ,IRNSS-1E ,IRNSS-1D ,IRNSS-1C ,IRNSS-1B ,IRNSS-1A ,INSAT 4CR ,INSAT 4B ,INSAT 4A ,INSAT 3DR ,INSAT 3D ,IRNSS-1I ,IRS-P6 ,Swayam,SRMSat ,South Asia Satellite ,ScatSat-1,RISat-2B ,RISat-2 ,RISat-1 ,Resourcesat-2A,PISAT ,NIUSat ,Microsat-TD,Kalpana-1 ,INS-1C ,INS-1B ,INS-1A ,GSAT-11,GSAT-10,EMISat ,CartoSat 2F,CartoSat 2E,CartoSat 2D,CartoSat 2C,CartoSat 2B,CartoSat 2A,CartoSat 1 ,Astrosat,ExSeedSat-1 ,GSAT-12,GSAT-14,HySIS ,GSAT-8,GSAT-7A,GSAT-7,GSAT-6 ,GSAT-31,GSAT-29,GSAT-19E,GSAT-18,GSAT-17,GSAT-16,GSAT-15,CartoSat 2 ",
                    "users":"Government,Civil,Military,Commercial,Government/Commercia",
                    "vid_link":"https://www.youtube.com/embed/IgAnj6r1O48"}
        for k in correct:
            assert correct[k] == response_json[k]


    def test_14(self):
        response = self.app.get('/api/satellites/5')
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        correct = {"apogee_km":"\"36,013\"",
                    "astronauts":"Alan Bean,Alan G. Poindexter,Alan Shepard,Albert Sacco,Alfred Worden,Andrew J. Feustel,Andrew M. Allen,Andrew R. Morgan,Andrew Thomas,Anna Lee Fisher, M.D.,Anne McClain,Anthony W. England,B. Alvin Drew,Barbara Morgan,Barry Wilmore,Bernard A. Harris, Jr.,Bonnie J. Dunbar,Brent W. Jett, Jr.,Brewster Shaw,Brian Binnie,Brian Duffy,Bruce McCandless II,Bruce Melnick,Bryan OConnor,Buzz Aldrin,Byron Lichtenberg,Carl Meade,Carl Walz,Catherine Coleman,Charles Bolden,Charles Camarda,Charles D. Walker,Charles Duke,Charles E. Brady, Jr., M.D.,Charles Fullerton,Charles Gemar,Charles Hobaugh,Charles Pete Conrad,Charles Precourt,Charles Veach,Christina Koch,Christopher Cassidy,Christopher Ferguson,Clayton Anderson,Curtis Brown,Dale Gardner,Daniel Barry,Daniel Brandenstein,Daniel Burbank,Daniel Bursch,Daniel Tani,David Griggs,David Hilmers,David Leestma,David M. Brown,David M. Walker,David Scott,David Wolf, M.D.,Dennis Tito,Dick Scobee,Dominic A. Antonelli,Dominic Gorie,Don Lind,Donald Deke Slayton,Donald McMonagle,Donald Pet",
                    "class_of_orbit":"GEO",
                    "country":"United States",
                    "date_of_launch":"5/9/1998",
                    "expected_lifetime_yr":"N/A",
                    "img_link":"https://tse4.mm.bing.net/th?id=OIP.7KK_ibd2vPUKt-mewG7EFgAAAA&pid=Api",
                    "launch_mass_kg":"\"4,500\"",
                    "launch_site":"Cape Canaveral",
                    "launch_vehicle":"Titan IVA",
                    "longitude_degree":"-26.00",
                    "operator":"National Reconnaissance Office (NRO)",
                    "perigee_km":"\"35,560\"",
                    "period_minute":"1436.14",
                    "purpose":"Earth Observation",
                    "sat_id":5,
                    "satellite":"Advanced Orion 2 ",
                    "source":"http://www.lib.cas.cz/www/space.40/1990/097B.HTM",
                    "summary":"Master of Orion is a turn-based, 4X science fiction strategy game released in 1993 by MicroProse on the MS-DOS and Mac OS operating systems. The game is the first in its franchise, and the rights are held by Wargaming. The player leads one of ten races to dominate the galaxy through a combination of... \nRead more at wikipedia: https://en.wikipedia.org/wiki/Master_of_Orion",
                    "users":"Military",
                    "vid_link":"https://www.youtube.com/embed/kng8w23imLg"}
        assert correct["astronauts"] == response_json["astronauts"]

    def test_15(self):
        response = self.app.get('/api/satellites/105')
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        correct = {"apogee_km":"\"23,382\"",
                    "astronauts":"N/A",
                    "class_of_orbit":"MEO",
                    "country":"ESA",
                    "date_of_launch":"3/27/2015",
                    "expected_lifetime_yr":"12",
                    "img_link":"http://1.bp.blogspot.com/-3n3FV3yHmGE/UenxsHtms-I/AAAAAAAAIZo/sKuQGxTWY4U/s1600/TDRS+satellite.jpg",
                    "launch_mass_kg":"723",
                    "launch_site":"Guiana Space Center",
                    "launch_vehicle":"Soyuz-ST",
                    "longitude_degree":"0.00",
                    "operator":"European Space Agency (ESA)",
                    "perigee_km":"\"23,353\"",
                    "period_minute":"850.5",
                    "purpose":"Navigation/Global Positioning",
                    "sat_id":105,
                    "satellite":"Galileo FOC FM4 ",
                    "source":"http://www.insidegnss.com/node/4471",
                    "summary":"This is a list of past and present satellites of the Galileo navigation system.  \nThe two GIOVE prototype vehicles were retired in 2012.\nRead more at wikipedia: https://en.wikipedia.org/wiki/List_of_Galileo_satellites",
                    "users":"Commercial",
                    "vid_link":"https://www.youtube.com/embed/0XJDKPcrGqc"}
        for k in correct:
            assert correct[k] == response_json[k]

#----------------------------TEST SPECIFIC INFO-------------------------------

    def test_16(self):
        response = self.app.get('/api/countries')
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        counter = 0
        for d in response_json["objects"]:
            if 'military' in d['users'].lower():
                counter += 1
        self.assertEqual(counter, 26)

    def test_17(self):
        response = self.app.get('/api/astronauts')
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        counter = 0
        for d in response_json["objects"]:
            if 'japan' in d['country'].lower():
                counter += 1
        self.assertEqual(counter, 11)

    def test_18(self):
        response = self.app.get('/api/satellites')
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        counter = 0
        for d in response_json["objects"]:
            if 'technology' in d['purpose'].lower():
                counter += 1
        self.assertEqual(counter, 300)

    def test_19(self):
        response = self.app.get('/api/astronauts/Akihiko%20Hoshide')
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        self.assertEqual(response.get_json()['total_flight_time'], '140:17:26')

    def test_20(self):
        response = self.app.get('/api/countries/china')
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        self.assertEqual(response.get_json()['count_satellites'], 308)

#----------------------------TEST 404S-------------------------------

    def test_21(self):
        response = self.app.get('/api/astronauts/mickeymouse')
        self.assertEqual(response.status_code, 404)

    def test_22(self):
        response = self.app.get('/api/astronauts/genekranz')
        self.assertEqual(response.status_code, 404)

    def test_23(self):
        response = self.app.get('/api/satellites/ussenterprise')
        self.assertEqual(response.status_code, 404)

    def test_24(self):
        response = self.app.get('/api/satellites/departmentofdefense')
        self.assertEqual(response.status_code, 404)

    def test_25(self):
        response = self.app.get('/api/countries/panem')
        self.assertEqual(response.status_code, 404)


if __name__ == '__main__':
    unittest.main()
