import os
from dotenv import load_dotenv
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import flask_restless
from flask_cors import CORS
from models import *

load_dotenv()

application = app = Flask(__name__)
CORS(app)
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = os.getenv("SQLALCHEMY_DATABASE_URI")
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db.init_app(app)

# with app.app_context():
#     db.create_all()

# creates the Flask-Restless API manager
manager = flask_restless.APIManager(app, flask_sqlalchemy_db=db)

# Create API endpoints at /api/<tablename> by default

# http://127.0.0.1:5000/api/astronauts
astronauts = manager.create_api(model=Astronauts, methods=["GET"], results_per_page=0)

# http://127.0.0.1:5000/api/spaceXrockets
rockets = manager.create_api(model=SpaceXrockets, methods=["GET"], results_per_page=0)

# http://127.0.0.1:5000/api/countries
countries = manager.create_api(model=Countries, methods=["GET"], results_per_page=0)

# http://127.0.0.1:5000/api/satellites
satellites = manager.create_api(model=Satellites, methods=["GET"], results_per_page=0)


def main():
    astros = Astronauts.query.all()
    for astro in astros:
        print(astro.name)
    """ rockets = SpaceXrockets.query.all()
    for rocket in rockets:
        print(rocket.rocket_name) """


if __name__ == "__main__":
    # main()
    # To start the flask loop we will eventually wanna do this ->
    # application.debug = True
    # application.run()
    application.run(host="0.0.0.0")
