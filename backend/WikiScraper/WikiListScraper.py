import wikipedia
import json

def scrape(listPageTitle, category, filter):
    data = list()
    fileName = category + "Cache.json"

    try:
        # Try to load the cache file
        print("Loading " + category +" Cache")
        with open(fileName, 'r') as loadcache:
            data = json.load(loadcache)
    except (json.decoder.JSONDecodeError, IOError):
        # Loading hte cache file failed
        print("Cache not avalable")
        print("Getting data...")
        print("This may take a while...")
        
        
        # Initalize source page and progess reasources
        page = wikipedia.page(title=listPageTitle)
        count = 0
        total = len(page.links)
        
        # Crawl pages
        for link in page.links:
            count += 1
            try:
                # Attempt to get astronaut page
                print("Trying " + link + " (" + str(count) + "/" + str(total) + ")")
                subPage = wikipedia.page(title=link)
                pageText = subPage.content
                
                #Checks if all are true
                result =  all(elem in pageText for elem in filter) 
                
                if result:
                    # Page refers to category object
                    data.append(link)
                else:
                    # Page does not refer to category object
                    print("This is not a " + category)
            except Exception as e:
                # Failed to access page for some reason
                print("Error " + link)
                print(type(e))
                print(e)
        print(data)
        with open(fileName, 'w') as outfile:
            js = json.dump(data, outfile)

    # data Dictionary created at this point
    return data

if __name__== "__main__":
    scrape()


