import wikipedia
import json

def scrape(listPageTitle, category, filter, remove):
    data = list()
    fileName = category + "Cache.json"

    
        # Initalize source page and progess reasources
    page = wikipedia.page(title=listPageTitle)
    count = 0
    total = len(page.links)
    
    # Crawl pages
    for link in page.links:
        count += 1
        try:
            # Attempt to get astronaut page
            print("Trying " + link + " (" + str(count) + "/" + str(total) + ")")
            subPage = wikipedia.page(title=link)
            pageText = subPage.content
            
            #Checks if all are true
            result = all(elem in pageText for elem in filter) and all(elem not in pageText for elem in remove)
            
            if result:
                # Page refers to category object
                data.append(link)
            else:
                # Page does not refer to category object
                print("This is not a " + category)
        except Exception as e:
            # Failed to access page for some reason
            print("Error " + link)
            print(type(e))
            print(e)
    print(data)
    with open(fileName, 'w') as outfile:
        js = json.dump(data, outfile)

if __name__== "__main__":
    scrape("List of space travelers by name", "Astronauts", ["born"], ["flag", "capital", "Flag", "Capital"  ])
    scrape("List of sovereign states", "Countries", ["flag"], [])