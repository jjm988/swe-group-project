import wikipedia

import urllib.request, json

import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode



def get_country():
    try:
        connection = mysql.connector.connect(host='landitinspacedb.ckq85yquf2gl.us-east-1.rds.amazonaws.com',
                                             database='landitinspaceDB',
                                             user='backend',
                                             password='1234')

        query = 'SELECT country FROM countries'

        cursor = connection.cursor(buffered=True)
        cursor.execute(query)
        connection.commit()
        records = cursor.fetchall()
        it = iter(records)
        countries = []
        for row in it:
            countries.append(row[0])
        cursor.close()
        return countries


    except mysql.connector.Error as error:
        print("Failed to execute a query {}".format(error))

def country_summary():
    cache = dict()
    fileName = "countrySumV2.json"
    countries = get_country()

    dic = dict()
    country_itr = iter(countries)

    for country in country_itr:
        search_key = country.split('/')
        char_limit = 300
        if len(search_key) > 1:
            char_limit = 150
        sum = ''
        for c in search_key:
            if c in cache:
                summary = cache[c]
            else:
                try:
                    wiki_page = wikipedia.page(title='country ' + c, pageid=None, auto_suggest=True, redirect=True, preload=False)
                except wikipedia.exceptions.PageError:
                    pass
                except wikipedia.exceptions.DisambiguationError:
                    choices = wikipedia.search(search_key)
                    print ("Recommendation may refer to: ")
                    for i, topic in enumerate(choices):
                        print (i,' ', topic)
                    choice = int(input("Enter a choice: "))
                    # summary = wikipedia.summary(choices[choice])
                    wiki_page = wikipedia.page(title=choices[choice], pageid=None, auto_suggest=True, redirect=True, preload=True)

                summary = wiki_page.summary

                if len(summary) > char_limit:
                    summary = summary[0:char_limit]
                    summary += '... \nRead more at wikipedia: ' + wiki_page.url
                else:
                    summary += '\nRead more at wikipedia: ' + wiki_page.url


                cache[c] = summary

            sum += '\n' + summary
        print(country, " ", sum)
        dic[country] = sum

    with open(fileName, 'w') as outfile:
        js = json.dump(dic, outfile)


# def modify_json():
#     f = open('countrySumV2.json', 'r')
#
#     # returns JSON object as
#     # a dictionary
#     data = json.load(f)
#     f.close()
#
#     f = open('countrySumV2.json', 'w')
#
#     for i in data:
#         countries = i.split('/')
#         for country in countries:
#             if country.lower() == "china":
#                 summary = "China (Chinese: 中国; pinyin: Zhōngguó; literally: 'Middle State'), "
#                 "officially the People's Republic of China (PRC) (Chinese: 中华人民共和国; pinyin: Zhōnghuá Rénmín Gònghéguó), "
#                 "is a country in East Asia. It is the world's most populous country, with a population of around 1.428 billion "
#                 "in 2017. Covering approximately 9,600,000 square kilometers (3,700,000 sq mi), it is the world's third-largest country by area."
#                 "\nRead more at wikipedia: https://en.wikipedia.org/wiki/China"
#
#                 taiwan = "\nTaiwan, officially the Republic of China (ROC), is a state in East Asia. "
#                 "Neighbouring states include the People's Republic of China (PRC) to the north-west, "
#                 "Japan to the north-east, and the Philippines to the south. The island of Taiwan has "
#                 "an area of 35,808 square kilometers (13,826 sq mi), with mo... \nRead more at wikipedia: "
#                 "https://en.wikipedia.org/wiki/Taiwan"
#                 data[i] = data[i].replace(taiwan, summary)
#                 json.dump(data, f)
#                 print (i, ' ', data[i])
#     f.close()

def get_ESA():
    wiki_page = wikipedia.page(title='France', pageid=None, auto_suggest=True, redirect=True, preload=False)
    char_limit = 300
    summary = wiki_page.summary

    if len(summary) > char_limit:
        summary = summary[0:char_limit]
        summary += '... \nRead more at wikipedia: ' + wiki_page.url
    else:
        summary += '\nRead more at wikipedia: ' + wiki_page.url
    print(summary)

if __name__== "__main__":
    get_ESA()
    # main()
