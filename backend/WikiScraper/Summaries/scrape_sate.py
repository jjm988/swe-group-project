import wikipedia

import urllib.request, json

import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode



def get_sates():
    try:
        connection = mysql.connector.connect(host='landitinspacedb.ckq85yquf2gl.us-east-1.rds.amazonaws.com',
                                             database='landitinspaceDB',
                                             user='backend',
                                             password='1234')

        query = 'SELECT satellite FROM satellites'

        cursor = connection.cursor(buffered=True)
        cursor.execute(query)
        connection.commit()
        records = cursor.fetchall()
        it = iter(records)
        sates = []
        for row in it:
            sates.append(row[0])
        cursor.close()
        # return countries
        print(sates)
        with open("satellite_cache.json", 'w') as outfile:
            js = json.dump(sates, outfile)

    except mysql.connector.Error as error:
        print("Failed to execute a query {}".format(error))

def sate_summary():
    cache = dict()
    fileName = "satelliteSumV2.json"

    f = open('satellite_cache.json', 'r')

    # list of satellite names
    sates = json.load(f)
    f.close()



    dic = dict()
    sate_itr = iter(sates)

    for sate in sate_itr:
        print(sate)
        char_limit = 300

        while True:
            search_key = sate
            try:
                wiki_page = wikipedia.page(title=search_key, pageid=None, auto_suggest=True, redirect=True, preload=False)
                break
            except wikipedia.exceptions.PageError:
                wiki_page = False
                break
            except wikipedia.exceptions.DisambiguationError:
                choices = wikipedia.search(search_key)
                print ("Recommendation may refer to: ")
                for i, topic in enumerate(choices):
                    print (i,' ', topic)
                choice = input("Enter a choice: ")
                if choice == 'n':
                    # summary = sate + " is an object that has been intentionally placed into orbit. These objects are called artificial satellites to distinguish them from natural satellites such as Earth's Moon.\nRead more at wikipedia: https://en.wikipedia.org/wiki/Satellite"
                    # dic[sate] = summary
                    wiki_page = False
                    break
                else:
                    choice = int(choice)
                    sate = choices[choice]


        if wiki_page==False:
            summary = sate + " is an object that has been intentionally placed into orbit. These objects are called artificial satellites to distinguish them from natural satellites such as Earth's Moon.\nRead more at wikipedia: https://en.wikipedia.org/wiki/Satellite"
        else:
            summary = wiki_page.summary
            if len(summary) > char_limit:
                summary = summary[0:char_limit]
                summary += '... \nRead more at wikipedia: ' + wiki_page.url
            else:
                summary += '\nRead more at wikipedia: ' + wiki_page.url

        print(summary)
        dic[sate] = summary

    with open(fileName, 'w') as outfile:
        js = json.dump(dic, outfile)


if __name__== "__main__":
    sate_summary()
        # main()
