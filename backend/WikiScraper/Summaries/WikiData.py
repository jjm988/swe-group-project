import wikipedia
from WikiListScraper import scrape
import urllib.request, json 

#For infoBox Scraping
import wptools

class data:
    # Store the Model data in the object
    def __init__(self, page, type, filter):
        self.data = scrape(page, type, filter)
    
    # Iterator over the page names
    def __iter__(self):
        return iter(self.data)
        
    # Returns the Page object given a name
    def getPage(self, name):
        return wikipedia.page(name)
      
    # Returns the Summary given the name
    def getSummary(self, name):
        return self.getPage(name).summary
        
    # Returns all images from the page
    def getImageURLs(self, name):
        return self.getPage(name).images
    
    #Returns a dictionary of the wiki infobox.
    def getInfoBox(self, name):
        so = wptools.page(name).get_parse()
        infobox = so.data['infobox']
        return infobox
        
    #Returns the url of the image for the page
    def getImageURL(self, name):
        url = 'https://en.wikipedia.org/wiki/' + name.replace(" ", "_") + '#/media/' + wptools.page(name).get_parse().data['infobox']['image']
        return url