import wikipedia
from WikiListScraper import scrape
from WikiData import data
import urllib.request, json

#For infoBox Scraping
import wptools

def astronauts():
    fileName = "astroSum"
    info = data("List of space travelers by name", "Astronauts", ["born"])
    dic = dict()
    for name in info:
        dic[name] = info.getSummary(name)
    with open(fileName, 'w') as outfile:
        js = json.dump(dic, outfile)
    
def countries():
    fileName = "countSum"
    info = data("List of sovereign states", "Countries", ["flag"])
    dic = dict()
    for name in info:
        dic[name] = info.getSummary(name)
    with open(fileName, 'w') as outfile:
        js = json.dump(dic, outfile)
    
def satellites():
    pass

if __name__== "__main__":
    countries()
    astronauts()
    pass
