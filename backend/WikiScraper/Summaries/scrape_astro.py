import wikipedia

import urllib.request, json

import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode



def get_astro_names():
    try:
        connection = mysql.connector.connect(host='landitinspacedb.ckq85yquf2gl.us-east-1.rds.amazonaws.com',
                                             database='landitinspaceDB',
                                             user='backend',
                                             password='1234')

        query = 'SELECT DISTINCT(name) FROM astronauts'

        cursor = connection.cursor(buffered=True)
        cursor.execute(query)
        connection.commit()
        records = cursor.fetchall()
        it = iter(records)
        astro_names = []
        for row in it:
            astro_names.append(row[0])
        cursor.close()
        return astro_names


    except mysql.connector.Error as error:
        print("Failed to execute a query {}".format(error))

def astronauts():
    fileName = "astroSumV2"
    astro_names = get_astro_names()

    dic = dict()
    astro_it = iter(astro_names)

    for name in astro_it:
        search_key = name+' astronaut'
        try:
            wiki_page = wikipedia.page(title=search_key, pageid=None, auto_suggest=True, redirect=True, preload=False)
        except wikipedia.exceptions.PageError:
            pass
        except wikipedia.exceptions.DisambiguationError:
            choices = wikipedia.search(search_key)
            print ("Recommendation may refer to: ")
            for i, topic in enumerate(choices):
                print (i,' ', topic)
            choice = int(input("Enter a choice: "))
            wiki_page = wikipedia.page(title=choices[choice], pageid=None, auto_suggest=True, redirect=True, preload=True)

        summary = wiki_page.summary
        if len(summary) > 300:
            summary = summary[0:300]
            summary += '... \nRead more at wikipedia: ' + wiki_page.url
        else:
            summary += '\nRead more at wikipedia: ' + wiki_page.url
        print(summary)
        dic[name] = summary

    with open(fileName, 'w') as outfile:
        js = json.dump(dic, outfile)





if __name__== "__main__":
    # get_astro_names()
    # astronauts()
