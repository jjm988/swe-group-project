import wikipedia
from WikiListScraper import scrape
import urllib.request, json 

#For infoBox Scraping
import wptools

class data:
    def __init__(self, page, type, filter):
        self.data = scrape(page, type, filter)
    
    def __iter__(self):
        return iter(self.data)
        
    def getPage(self, name):
        return wikipedia.page(name)
        
    def getSummary(self, name):
        return self.getPage(name).summary
        
    def getImageURLs(self, name):
        return self.getPage(name).images
    
    #Returns a dictionary of the wiki infobox.
    def getInfoBox(self, name):
        so = wptools.page(name).get_parse()
        infobox = so.data['infobox']
        return infobox
        
    #Returns the url of the image for the page
    def getImageURL(self, name):
        url = 'https://en.wikipedia.org/wiki/' + name.replace(" ", "_") + '#/media/' + wptools.page(name).get_parse().data['infobox']['image']
        return url
        
def initAllData():
    astronauts = data("List of space travelers by name", "astronauts", ["born"])
    rockets = data("List of orbital launch systems", "rockets", ["launch", "rocket"])
    models = dict()
    models["astronauts"] = astronauts
    models["rockets"] = rockets
    return models
    
if __name__== "__main__":
    models = initAllData()