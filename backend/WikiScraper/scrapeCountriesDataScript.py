import json


filename = 'data.txt'

dict1 = []

fields =["NameofSatellite,AlternateNames",	"Country/OrgofUNRegistry",	"CountryofOperator/Owner",	"Operator/Owner",	"Users",	"Purpose",	"DetailedPurpose",	"ClassofOrbit",	"TypeofOrbit",	"LongitudeofGEO(degrees)",	"Perigee(km)",	"Apogee(km)",	"Eccentricity",	"Inclination(degrees)",	"Period(minutes)",	"LaunchMass(kg.)",	"DryMass(kg.)",	"Power(watts)",	"DateofLaunch",	"ExpectedLifetime yrs.)",	"Contractor",	"CountryofContractor",	"LaunchSite",	"LaunchVehicle",	"COSPARNumber",	"NORADNumber",	"Comments",		"", "SourceUsedforOrbitalData",	"Source",	"Source",	"Source",	"Source",	"Source",	"Source"]

with open(filename) as fh:

    i = 0

    l = 1

    for line in fh:

        description = list(line.strip().split("\t"))
        print(len(description))
        print(len(fields))
        dict2 = {}
        i = 0
        small = len(description) if len(description)<len(fields) else len(fields)
        while i<small:
                dict2[fields[i]]= description[i]
                i = i + 1
        print(dict2)
        dict1.append(dict2)
        print(len(dict1))
        l = l + 1

out_file = open("satelliteData.json", "w")
json.dump(dict1, out_file, indent = 4)
out_file.close()
