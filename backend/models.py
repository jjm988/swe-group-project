from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Astronauts(db.Model):
    __tablename__ = 'astronauts'
    name = db.Column(db.String(40), primary_key = True)
    country = db.Column(db.String(30))
    gender = db.Column(db.String(10))
    flights = db.Column(db.String(45))
    total_flights = db.Column('total flights', db.Integer)
    total_flight_time = db.Column('total flight time', db.String(30))
    satellites = db.Column(db.String(2000))
    purposes = db.Column(db.String(1000))
    users = db.Column(db.String(1000))
    operators = db.Column(db.String(2000))
    img_link = db.Column(db.String(500))
    vid_link = db.Column(db.String(500))
    summary = db.Column(db.String(500))

class SpaceXrockets(db.Model):
    __tablename__ = 'spaceXrockets'
    rocket_id = db.Column(db.String(20), primary_key = True)
    rocket_name = db.Column(db.String(20))
    rocket_type = db.Column(db.String(20))
    first_flight = db.Column(db.String(45))
    company = db.Column(db.String(20))
    active = db.Column(db.String(10))
    country = db.Column(db.String(40))
    cost_per_launch = db.Column(db.Integer)

class Countries(db.Model):
    __tablename__ = 'countries'
    country = db.Column(db.String(100), primary_key =True)
    satellites = db.Column(db.String(2000))
    count_satellites = db.Column(db.Integer)
    astronauts = db.Column(db.String(2000))
    count_astronauts = db.Column(db.Integer)
    operators = db.Column(db.String(2000))
    launch_sites = db.Column(db.String(2000))
    launch_vehicles = db.Column(db.String(2000))
    purposes = db.Column(db.String(1000))
    users = db.Column(db.String(1000))
    img_link = db.Column(db.String(500))
    vid_link = db.Column(db.String(500))
    summary = db.Column(db.String(1100))

class Satellites(db.Model):
    __tablename__ = 'satellites'
    sat_id = db.Column(db.Integer, primary_key = True)
    satellite = db.Column(db.String(100))
    longitude_degree = db.Column(db.String(40))
    expected_lifetime_yr = db.Column(db.String(10))
    source = db.Column(db.String(150))
    country = db.Column(db.String(40))
    astronauts = db.Column(db.String(2000))
    apogee_km = db.Column(db.String(20))
    users = db.Column(db.String(20))
    launch_mass_kg = db.Column(db.String(20))
    purpose = db.Column(db.String(30))
    operator = db.Column(db.String(40))
    class_of_orbit = db.Column(db.String(10))
    period_minute = db.Column(db.String(15))
    launch_site = db.Column(db.String(50))
    perigee_km = db.Column(db.String(20))
    date_of_launch = db.Column(db.String(20))
    launch_vehicle = db.Column(db.String(30))
    img_link = db.Column(db.String(500))
    vid_link = db.Column(db.String(500))
    summary = db.Column(db.String(500))
