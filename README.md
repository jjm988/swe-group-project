<pre>
Group members:

Name                 eid            gitlab id    estimated completion time     actual completion time
----------------------------------------------------------------------------------------------------
Smruti Shah         scs3629         shahsmruti        30 hours                      35 hours
Jiaming Ji          jj35553         jjm988            30 hours                      33 hours
Namit Agrawal       nra544          namit.agrawal     42 hours                      41 hours
Evan Hodde          emh3297         evanhodde20       37 hours                      43 hours
Sonali Bhat         sb53536         sonali17          44 hours                      46 hours
Chinmayee Kulkarni  csk767          ckulkarni5        32 hours                      36 hours


Git SHA: e74d7bac56028296584135e574e148f084e36b26

Project Leader: Sonali Bhat

Gitlab Pipelines: https://gitlab.com/jjm988/swe-group-project/pipelines

Website Link: https://www.landitinspace.com/

Comments:
test.js file is under swe-group-project/frontend/test/ folder

</pre>
