from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello to the World of Flask!'

@app.route('/about')
def about():
    return render_template("")

if __name__ == '__main__':
    app.run()
